/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef SIG_H
#define SIG_H

#include "print.h"

#if HAVE_LIBB2
#	include <blake2.h>
#else
#	define BLAKE2B_OUTBYTES 64
#endif

#if HAVE_LIBCRYPTO
#	include <openssl/opensslv.h>
#	if OPENSSL_VERSION_NUMBER >= 0x10100000L
#		include <openssl/evp.h>
#	endif
#endif

enum
{
	FLAG_FAT = 1 << 0,
	FLAG_FAT32 = 1 << 1,
	FLAG_EXFAT = 1 << 2,
	FLAG_NTFS = 1 << 3,
	FLAG_STR_FF = 1 << 4,
	FLAG_STR8_DELTA = 1 << 5,
};

struct chunk_addr
{
	uint16_t begin;
	uint16_t end;
};

struct chunk_literal
{
	struct chunk_addr addr;
	const void *data;
};

struct chunk_hash
{
	struct chunk_addr addr;
	char blake2b[BLAKE2B_OUTBYTES];
};

struct signature_base
{
	const struct chunk_literal *literals;
	const struct chunk_hash *hashes;
	unsigned char literals_size;
	unsigned char hashes_size;
};

struct signature_full
{
	struct signature_base base;

	const _TCHAR *system;
	const uint16_t *str16;
	uint16_t str8_offset;
	// uint16_t str8_base;
	uint16_t str16_base;
	uint8_t str8_n;
	uint8_t str16_size;
	uint8_t flags;
};

struct signature_grub2_boot
{
	struct signature_full full;
	struct signature_base floppy;
	uint8_t core_sector;
	uint8_t boot_drive;
	uint8_t after_bpb;
};

struct signature_grub1_9_kernel
{
	struct signature_full full;
};

struct signature_grub2_lzma_decompress
{
	struct signature_full full;
	struct signature_base grub_gate_a20;
	struct signature_base post_reed_solomon;
	uint16_t call_grub_gate_a20;
	uint16_t jmp_post_reed_solomon;
	uint16_t post_reed_solomon_decompressor_end;
};

struct signature_grub2_kernel
{
	struct signature_full full;
	uint32_t size;
	// TODO: The other elements here.
	// uint32_t size3000;
	// uint32_t bss_start;
};

void blake2b512(uint8_t *out, const void *in, size_t inlen);

bool str_valid(const void *sector, unsigned offset, bool ff);
bool str16_valid(const void *sector, unsigned offset, unsigned base, bool ff);

unsigned signature_begin(const struct signature_base *sig);
unsigned signature_end(const struct signature_base *sig);

bool check_signature_base(const struct signature_base *sig, const void *sector);
bool check_signature(const _TCHAR *class, const struct signature_full *sig, uint64_t sector_offset, const void *sector);

#endif
