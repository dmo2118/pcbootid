/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "os.h"

struct _int13_error_message
{
	uint8_t status;
	const char *what;
};

void print_error(error_status error, print_func print)
{
	static const struct _int13_error_message messages[] =
	{
		{0x00, "Successful completion"},
		{0x01, "Invalid function in AH or invalid parameter"},
		{0x02, "Address mark not found"},
		{0x03, "Disk write-protected"},
		{0x04, "Sector not found/read error"},
		{0x05, "Reset failed"},
		{0x06, "Disk changed"},
		{0x07, "Drive parameter activity failed"},
		{0x08, "DMA overrun"},
		{0x09, "Data boundary error"},
		{0x0A, "Bad sector detected"},
		{0x0B, "Bad track detected"},
		{0x0C, "Unsupported track or invalid media"},
		{0x0D, "Invalid number of sectors on format"},
		{0x0E, "Control data address mark detected"},
		{0x0F, "DMA arbitration level out of range"},
		{0x10, "Uncorrectable CRC or ECC error on read"},
		{0x11, "Data ECC corrected"},
		{0x20, "Controller failure"},
		{0x31, "No media in drive"},
		{0x32, "Incorrect drive type stored in CMOS"},
		{0x40, "Seek failed"},
		{0x80, "Timeout (not ready)"},
		{0xAA, "Drive not ready"},
		{0xB0, "Volume not locked in drive"},
		{0xB1, "Volume locked in drive"},
		{0xB2, "Volume not removable"},
		{0xB3, "Volume in use"},
		{0xB4, "Lock count exceeded"},
		{0xB5, "Valid eject request failed"},
		{0xB6, "Volume present but read protected"},
		{0xBB, "Undefined error"},
		{0xCC, "Write fault"},
		{0xE0, "Status register error"},
		{0xFF, "Sense operation failed"},
	};

	unsigned lo = 0, hi = arraysize(messages);
	const char *message;
	for(;;)
	{
		unsigned mid;
		uint8_t key;
			
		if(lo == hi)
		{
			message = "Unknown INT 13 error";
			break;
		}

		mid = (lo + hi) / 2u;
		key = messages[mid].status;

		if(key == error)
		{
			message = messages[mid].what;
			break;
		}
			
		if(key < error)
			lo = mid + 1;
		else
			hi = mid;
	}

	print(message);
}

struct _int13e_device_address_packet
{
	uint8_t packet_size;
	uint8_t reserved0;
	uint8_t number_of_blocks;
	uint8_t reserved1;
	void __far *transfer_buffer;
	uint64_t start_address;
};

uint8_t _int13_read(uint8_t drive_number, uint16_t cylinder_sector, uint8_t head, uint8_t number_of_sectors, void __far *buffer);

#pragma aux _int13_read = \
	"	mov ah, 02h" \
	"	stc" \
	"	int 13h" \
	"	sti" \
	"	jc end" \
	"	xor ah, ah" \
	"end:" \
	parm [dl] [cx] [dh] [al] [es bx] \
	value [ah];

static error_status _read_int13(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size)
{
	uint8_t sector, head;
	uint16_t cylinder;

	assert(disk->_sectors <= 63);
	assert(disk->_heads <= 256);

	sector = (uint8_t)(offset % disk->_sectors) + 1;
	offset /= disk->_sectors;
	head = (uint8_t)(offset % disk->_heads);
	offset /= disk->_heads;
	assert(offset < 0x400);
	cylinder = (uint16_t)offset; // TODO: Proper error handling here w.r.t. truncation.

	return _int13_read(disk->_drive, (cylinder << 8) | ((cylinder >> 2) & 0xC0) | sector, head, size, buffer);
}

uint8_t _int13e_read(uint8_t drive_number, struct _int13e_device_address_packet __near *);

#pragma aux _int13e_read = \
	"	mov ah, 42h" \
	"	int 13h" \
	"	jc done" \
	"	xor ah, ah" \
	"done:" \
	parm [dl] [si] \
	value [ah];

static error_status _read_int13e(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size)
{
	struct _int13e_device_address_packet packet;
	assert(size <= 0x80);
	packet.packet_size = sizeof(packet);
	packet.reserved0 = 0;
	packet.number_of_blocks = size;
	packet.reserved1 = 0;
	packet.transfer_buffer = buffer;
	packet.start_address = offset;
	_int13e_read(disk->_drive, &packet);
	// packet.number_of_blocks;
	return 0; // TODO: Error handling
}
