/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PCBOOTID_H
#define PCBOOTID_H

#if _WIN32
#	if defined _WINDOWS_ || defined _INC_TCHAR
#		error pcbootid.h must come before TCHAR/_TCHAR is defined.
#	endif
#	define _UNICODE 1   /* MSVC CRT */
#	define UNICODE 1    /* Win32 */
#endif

#if __unix__
#	if defined _STDLIB_H
#		error pcbootid.h must come before stdlib.h.
#	endif
#	define _FILE_OFFSET_BITS 64
#endif

#include <stdio.h>
#include <stdlib.h>

#if _WIN32 || __WATCOMC__
#include <tchar.h>

// Fix for Wine.
#undef _putts

static inline int _putts(const wchar_t *s)
{
	int ret = fputws(s, stdout);
	if(ret >= 0)
	{
		ret = putchar(L'\n');
		if(ret >= 0)
			ret = 0;
	}

	return ret;
}

#else
typedef char _TCHAR;

#define _T
#define _fputts     fputs
#define _puttc      putc
#define _puttchar   putchar
#define _putts      puts
#define _ftprintf   fprintf
#define _stprintf   sprintf
#define _tprintf    printf
#define _tmain      main
#define _tcstoul    strtoul
#endif

#if __STDC_VERSION__ < 199901
#	if _MSC_VER
#		define inline __inline
typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;

typedef unsigned __int32 uint_fast32_t;

#	elif __WATCOMC__
#		include <stdint.h>
#	else
#		error Unknown C89 compiler.
#	endif

typedef int bool;

enum
{
	false, true
};

#else
#	include <inttypes.h>
#	include <stdbool.h>
#endif

#ifndef static_assert
#	define static_assert(exp, s) typedef int _ASSERTION_FAILURE[exp ? 1 : -1]
#endif


#define arraysize(a) (sizeof(a) / sizeof(*(a)))
#define arrayend(a) ((a) + arraysize(a))

#define sector_size 512

static inline uint16_t _little_endian_load_16(const void *x)
{
	const uint8_t *x0 = (const uint8_t *)x;
	return x0[0] | (x0[1] << 8);
}

static inline uint32_t _little_endian_load_32(const void *x)
{
	const uint8_t *x0 = (const uint8_t *)x;
	return (uint32_t)x0[0] | ((uint32_t)x0[1] << 8) | ((uint32_t)x0[2] << 16) | ((uint32_t)x0[3] << 24);
/*
	TODO: Use
	return be32toh(*(uint32_t *__attribute__((packed)))x);
	...and other platform-dependent stuff.
*/
}

static inline uint64_t _little_endian_load_64(const void *x)
{
	const uint8_t *x0 = (const uint8_t *)x;
	return (uint64_t)x0[0] |
		((uint64_t)x0[1] << 8) |
		((uint64_t)x0[2] << 16) |
		((uint64_t)x0[3] << 24) |
		((uint64_t)x0[4] << 32) |
		((uint64_t)x0[5] << 40) |
		((uint64_t)x0[6] << 48) |
		((uint64_t)x0[7] << 56);
}

#endif
