#!/bin/sh

set -e

make -s clean
make -s

{
	cd image
	../pcbootid $(ls | grep -v TODO)
} | colordiff -F ^--- -u test.txt -
