/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CHECK_H
#define CHECK_H

#include "os.h"

enum
{
	EXIT_NO_ID = EXIT_FAILURE,
	EXIT_BOOT_CRASH = EXIT_FAILURE,
	EXIT_SYSTEM_ERROR = EXIT_FAILURE,
	EXIT_USAGE = 2 /* This could also be EX_USAGE from sysexits(3), but that's rarely used. */
	/*
	0/SUCCESS: Successful handoff to file system.
	1/FAILURE: Failure. Read the YAML for why:
	* NO_ID: {System: null}
	* BOOT_CRASH: {* OK: false}
	* SYSTEM_ERROR (I/O error, no memory): {Class: Error}
	2/USAGE: Bad command line usage.
	*/
};

int check_boot_record(disk_handle *disk, struct _disk_geometry *geometry, void *buffer, uint64_t sector_offset);
int check_grub_core(disk_handle *disk, struct _disk_geometry *geometry, void *buffer, uint64_t core_sector);

#endif
