/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "check.h"
#include "hashes.h"
#include "print.h"

#include <elf.h>
#include <lzma.h>

#include <assert.h>

// TODO: Does Digital Mars #define _M_I86 for 32-bit x86?
#ifdef _M_I86
#	include <malloc.h>

static inline const void __far *_far_from_huge(const void __huge *p)
{
	uint16_t off = FP_OFF(p);
	return MK_FP(FP_SEG(p) + (off >> 4), off & 0x000F);
}

static inline const void __far *_huge_add(const void __far *p, size_t bytes)
{
	uint32_t off = FP_OFF(p) + bytes;
	return MK_FP(FP_SEG(p) + (off >> 4), off & 0x000F);
}

#else
// TODO: halloc is calloc, not malloc. Do a malloc-like instead.
#	define huge_alloc malloc
#	define huge_free free
#	define __huge
#	define __far

static inline const void *_far_from_huge(const void *p)
{
	return p;
}

static inline const void *_huge_add(const void *p, size_t bytes)
{
	return p + bytes;
}

#endif

// From: include/grub/offsets.h, and previously:
// * include/grub/i386/pc/boot.h (as grub_boot_blocklist)
// * util/i386/pc/grub-setup.c (as boot_blocklist)

struct grub_pc_bios_boot_blocklist_32
{
	uint32_t start;
	uint16_t len;
	uint16_t segment;
} __attribute__((packed));

struct grub_pc_bios_boot_blocklist_64
{
	uint64_t start;
	uint16_t len;
	uint16_t segment;
} __attribute__((packed));

struct grub_module_info
{
	uint32_t magic;
	uint32_t offset;
	uint32_t size;
};

struct grub_module_header
{
	uint32_t type;
	uint32_t size;
};

enum
{
	OBJ_TYPE_ELF,
	OBJ_TYPE_MEMDISK,
	OBJ_TYPE_CONFIG,
	OBJ_TYPE_PREFIX,
	OBJ_TYPE_PUBKEY
};

enum
{
	GRUB_MODULE_MAGIC = 0x676d696d
};

static_assert(sizeof(struct grub_pc_bios_boot_blocklist_32) == 8, "sizeof(struct grub_pc_bios_boot_blocklist_32) != 8");
static_assert(sizeof(struct grub_pc_bios_boot_blocklist_64) == 12, "sizeof(struct grub_pc_bios_boot_blocklist_64) != 12");

static void _print_error_yaml_lzma(lzma_ret ret)
{
	static const _TCHAR *const errors[] =
	{
		_T("LZMA_OK"),
		_T("LZMA_STREAM_END"),
		_T("LZMA_NO_CHECK"),
		_T("LZMA_UNSUPPORTED_CHECK"),
		_T("LZMA_GET_CHECK"),
		_T("LZMA_MEM_ERROR"),
		_T("LZMA_FORMAT_ERROR"),
		_T("LZMA_OPTIONS_ERROR"),
		_T("LZMA_DATA_ERROR"),
		_T("LZMA_BUF_ERROR"),
		_T("LZMA_PROG_ERROR")
	};

	assert(ret < arraysize(errors));
	print_error_yaml_prefix();
	_putts(errors[ret]);
}

static int _compare_blocks(const void *a, const void *b)
{
	return
		((const struct grub_pc_bios_boot_blocklist_64 *)a)->segment -
		((const struct grub_pc_bios_boot_blocklist_64 *)b)->segment;
}

struct _grub_blocks
{
	uint_fast32_t size;
	void __huge *mem; // Allocated in *_blocklist, freed in *_kernel.
	const void __huge *data;
};

static int _grub_check_blocklist(
	struct _grub_blocks *block_result,
	disk_handle *disk,
	const struct grub_pc_bios_boot_blocklist_64 *rbegin,
	const struct grub_pc_bios_boot_blocklist_64 *rend,
	bool rend_ok)
{
	// TODO: No overlap with GRUB boot code.
	print_bool(_T("Blocklist block count OK"), rend_ok);
	if(!rend_ok)
		return EXIT_BOOT_CRASH;

	size_t blocklist_size = rbegin - rend;

	_putts(_T("  Blocklist:"));
	for(unsigned i = 0; i != blocklist_size; ++i)
	{
		const struct grub_pc_bios_boot_blocklist_64 *block = rbegin - i;
		_tprintf(
			_T("  - Sector: %llu\n    Length: %u\n    Address: %#.4x\n"),
			(unsigned long long)block->start,
			block->len,
			block->segment << 4);

		// TODO: No data > 512 KB.
		// TODO: No data < 0x600.
		// TODO: No data overlapping 0x8000-0x8200.
	}

	const unsigned load_seg = 0x820;
	unsigned block_end_seg = load_seg;

	// Sort the blocklist, then find where and how much data will be loaded next.
	// TODO: Prove this works.
	{
		struct grub_pc_bios_boot_blocklist_64 blocklist_sorted[(512 / sizeof(struct grub_pc_bios_boot_blocklist_32)) - 1];
		memcpy(blocklist_sorted, rend + 1, blocklist_size * sizeof(*blocklist_sorted));

		qsort(blocklist_sorted, blocklist_size, sizeof(*blocklist_sorted), _compare_blocks);
		for(unsigned i = 0; i != blocklist_size; ++i)
		{
			const struct grub_pc_bios_boot_blocklist_64 *block = &blocklist_sorted[i];
			if(block->segment > block_end_seg)
				break;

			// Segment: 16 bytes, 2^4
			// Sector: 512 bytes, 2^9
			unsigned new_block_end_seg = block->segment + (block->len << 5);
			if(new_block_end_seg > block_end_seg)
				block_end_seg = new_block_end_seg;
		}
	}

	// Add space for a sector preceding what this actually needs.
	block_result->size = (uint_fast32_t)(block_end_seg - load_seg) << 4;
	block_result->mem = huge_alloc(block_result->size + sector_size);
	if(!block_result->mem)
	{
		print_error_yaml(error_no_memory);
		return EXIT_SYSTEM_ERROR;
	}

	// Read blocks in order, clipped between load_seg and block_end_seg.
	for(unsigned i = 0; i != blocklist_size; ++i)
	{
		struct grub_pc_bios_boot_blocklist_64 block = rbegin[-i]; // TODO: Use memcpy for unaligned.

		if(block.segment < load_seg)
		{
			unsigned skip_sectors = (load_seg - block.segment) >> 5;
			block.start += skip_sectors;
			block.len -= skip_sectors;
			block.segment += skip_sectors << 5;
		}

		// TODO: Clipping the block_data's max size.
		error_status status = disk_read(
			disk,
			(char __huge *)block_result->mem + ((block.segment - (load_seg - 0x20)) << 4),
			block.start,
			block.len);

		if(!error_success(status))
		{
			huge_free(block_result->mem);
			print_error_yaml(status);
			return EXIT_SYSTEM_ERROR;
		}
	}

	return EXIT_SUCCESS;
}

struct _grub_module
{
	const void __huge *data;
	size_t size;
	const uint8_t __far *name;
	const uint8_t __far *name_end;
};

static inline bool _one_or_two(uint8_t x)
{
	return x == 1 || x == 2;
}

static uint32_t _fail_utf8(const uint8_t **p)
{
	++*p;
	return 0xfffd;
}

// TODO: Almost totally untested!
static uint32_t _read_utf8(const uint8_t **p, const uint8_t *end)
{
	const uint8_t *p0 = *p;

	assert(p0 != end);

	uint8_t c = *p0;

	if(c < 0x80)
	{
		*p = p0 + 1;
		return c;
	}

	unsigned n;
	if((c & 0xe0) == 0xc0)
		n = 1;
	else if((c & 0xf0) == 0xe0)
		n = 2;
	else if((c & 0xf8) == 0xf0)
		n = 3;
	else
		return _fail_utf8(p);

	++p0;
	if(p0 + n > end)
		return _fail_utf8(p);

	uint32_t result = c & ((1 << (6 - n)) - 1);
	if(!result)
		_fail_utf8(p);

	do
	{
		c = *p0;
		++p0;
		if((c & 0xc0) != 0x80)
			return _fail_utf8(p);
		result = (result << 6) | (c & 0x3f);
		--n;
	} while(n);

	if(result >= 0xd800 && result < 0xe000)
		return _fail_utf8(p);

	*p = p0;
	return result;
}

static inline bool _section_ok(const Elf32_Shdr __far *shdr, uint32_t elf_size)
{
	return _little_endian_load_32(&shdr->sh_offset) + _little_endian_load_32(&shdr->sh_size) <= elf_size;
}

static inline bool _is_section(
	const void *elf,
	const Elf32_Shdr __far *strings,
	const Elf32_Shdr __far *shdr,
	const char *name,
	unsigned name_size)
{
	uint32_t sh_offset = _little_endian_load_32(&strings->sh_offset);
	uint32_t sh_name = _little_endian_load_32(&shdr->sh_name);
	return
		sh_name + name_size <= _little_endian_load_32(&strings->sh_size) &&
		!memcmp((const char *)elf + sh_offset + sh_name, name, name_size);
}

static inline int _check_grub_kernel(struct _grub_blocks *block, uint64_t core_sector)
{
	unsigned i;

	// boot/i386/pc/startup_raw.S (2.00)
	//   include:
	//     - kern/i386/realmode.S
	//     - rs_decoder.h <- lib/reed_solomon.c (TODO: Take this out of the signature!)
	//     - boot/i386/pc/lzma_decode.S
	//   offsets:
	//     COMPRESSED_SIZE: .long 0x08
	//     UNCOMPRESSED_SIZE: .long 0x0c
	//     NO_REED_SOLOMON_REDUNDANCY: .long 0x10
	//     NO_REED_SOLOMON_LENGTH: .short 0x14
	//     BOOT_DEVICE: .byte[4] 0x18
	//     grub_gate_a20
	//     reed_solomon_part
	//     post_reed_solomon
	//     decompressor_end
	for(i = 0; i != arraysize(grub2_lzma_decompress); ++i)
	{
		const struct signature_grub2_lzma_decompress *sig = &grub2_lzma_decompress[i];

		if(signature_end(&sig->full.base) <= block->size && check_signature_base(&sig->full.base, block->data))
		{
			uint32_t grub_gate_a20 =
				_little_endian_load_32(block->data + sig->call_grub_gate_a20) +
				sig->call_grub_gate_a20 +
				4;

			uint32_t post_reed_solomon =
				_little_endian_load_32(block->data + sig->jmp_post_reed_solomon) +
				sig->jmp_post_reed_solomon +
				4;

			if(signature_end(&sig->grub_gate_a20) + grub_gate_a20 <= block->size &&
				check_signature_base(&sig->grub_gate_a20, block->data + grub_gate_a20) &&
				signature_end(&sig->post_reed_solomon) + post_reed_solomon <= block->size &&
				check_signature_base(&sig->post_reed_solomon, block->data + post_reed_solomon))
			{
				// TODO: Make sure all the attributes from hashes.json are in range.
				// TODO: Make sure ranges, grub_gate_a20, and post_reed_solomon do not overlap.
				print_sector(sig->full.system, _T("GRUB core.img (lzma_decompress.img)"), core_sector);

				uint32_t decompressor_end =
					_little_endian_load_32(block->data + post_reed_solomon + sig->post_reed_solomon_decompressor_end);

				_tprintf(_T("  grub_gate_a20: %#lx\n"), (unsigned long)grub_gate_a20 + 0x8200);
				_tprintf(_T("  post_reed_solomon: %#lx\n"), (unsigned long)post_reed_solomon + 0x8200);
				_tprintf(_T("  decompressor_end: %#lx\n"), (unsigned long)decompressor_end);

				uint32_t compressed_size = _little_endian_load_32(block->data + 0x8);
				uint32_t uncompressed_size = _little_endian_load_32(block->data + 0xc);
				_tprintf(
					_T("  Compressed size: %lu\n  Uncompressed size: %lu\n"),
					(unsigned long)compressed_size,
					(unsigned long)uncompressed_size);

				// TODO: GRUB_KERNEL_I386_PC_REED_SOLOMON_REDUNDANCY, GRUB_KERNEL_I386_PC_NO_REED_SOLOMON_LENGTH
				// TODO: GRUB_DECOMPRESSOR_I386_PC_BOOT_DEVICE
				// TODO: Verify compressed size against actual size.

				lzma_stream strm = LZMA_STREAM_INIT;
				lzma_options_lzma lzopts = {1 << 16, NULL, 0, 3, 0, 2};
				lzma_filter filters[] = {{LZMA_FILTER_LZMA1, &lzopts}, {LZMA_VLI_UNKNOWN, NULL}};

				void *new_block = huge_alloc(uncompressed_size);
				if(!new_block)
				{
					print_error_yaml(error_no_memory);
					huge_free(block->mem);
					return EXIT_SYSTEM_ERROR;
				}

				lzma_ret ret = lzma_raw_decoder(&strm, filters);
				if(ret)
				{
					_print_error_yaml_lzma(ret);
					huge_free(new_block);
					huge_free(block->mem);
					return EXIT_SYSTEM_ERROR;
				}

				strm.next_in = block->data + (decompressor_end - 0x8200);
				strm.avail_in = compressed_size;
				strm.next_out = new_block;
				strm.avail_out = uncompressed_size;
				ret = lzma_code(&strm, LZMA_RUN);
				if(ret)
				{
					_print_error_yaml_lzma(ret);
					lzma_end(&strm);
					huge_free(block->mem);
					huge_free(new_block);
					return EXIT_BOOT_CRASH;
				}

				lzma_end(&strm);

				huge_free(block->mem);
				block->mem = new_block;
				block->size = uncompressed_size - strm.avail_out;
				block->data = block->mem;

				if(strm.avail_in)
				{
					print_error_yaml(error_buffer_overflow);
					huge_free(block->mem);
					return EXIT_BOOT_CRASH;
				}

				return -1;
			}
		}
	}

	// grub-core/kern/i386/pc/startup.S (2.00)
	//   includes:
	//     - grub-core/kern/i386/int.S
	//   offsets:
	//     - _edata
	for(i = 0; i != arraysize(grub2_kernel); ++i)
	{
		const struct signature_grub2_kernel *sig = &grub2_kernel[i];

		if(signature_end(&sig->full.base) <= block->size &&
			check_signature(_T("GRUB core.img (kernel.img)"), &sig->full, core_sector, block->data))
		{
			// TODO: Verify the other values here.
			// TODO: All object types.
			uint32_t size = _little_endian_load_32((const uint32_t __huge *)(block->data + sig->size));
			_tprintf(_T("  Kernel size: %lu\n"), (unsigned long)size);

			const struct grub_module_info __huge *mi;
			const struct grub_module_header __huge *mh;
			const char __far *mh_end;

			uint32_t mi_size = block->size - size;
			bool mi_size_ok = mi_size >= sizeof(*mi);
			uint32_t mi_modules = 0;
			if(mi_size_ok)
			{
				mi = (const struct grub_module_info *)(block->data + size);

				uint32_t mh_offset = _little_endian_load_32(&mi->offset);
				uint32_t mh_size = _little_endian_load_32(&mi->size);

				mi_size_ok = mh_size <= mi_size && mh_offset <= mh_size;
				if(mi_size_ok)
				{
					const struct grub_module_header *mh_start =
						(const struct grub_module_header *)((const char *)mi + mh_offset);
					mh = mh_start;
					mh_end = _huge_add(mi, mi_size);

					for(;;)
					{
						uint32_t remaining = mh_end - (const char *)mh;
						if(!remaining)
							break;

						if(sizeof(*mh) > remaining)
						{
							mi_size_ok = false;
							break;
						}

						uint32_t size = _little_endian_load_32(&mh->size);
						if(size < sizeof(*mh) || size > remaining)
						{
							mi_size_ok = false;
							break;
						}

						++mi_modules;
						mh = _huge_add(mh, size);
					}

					if(mi_size_ok)
						mh = mh_start;
				}
			}

			print_bool(_T("Modules size OK"), mi_size_ok);
			if(!mi_size_ok)
				return EXIT_BOOT_CRASH;

			bool mi_magic_ok = _little_endian_load_32(&mi->magic) == GRUB_MODULE_MAGIC;
			print_bool(_T("Modules magic OK"), mi_magic_ok);
			if(!mi_magic_ok)
				return EXIT_BOOT_CRASH;

			struct _grub_module *modules = calloc(mi_modules, sizeof(struct _grub_module));
			if(!modules)
			{
				print_error_yaml(error_no_memory);
				return EXIT_FAILURE;
			}

			_putts(_T("  Modules:"));

			uint32_t mod = 0;
			while((const void __far *)mh != mh_end)
			{
				static const _TCHAR *const types[] =
				{
					_T("ELF"), // TODO: Pull .modname (str), .moddeps ([str]), .module_license (str) sections.
					_T("MEMDISK"), // TODO: At least the size.
					_T("CONFIG"), // TODO: Print me. Parse me for the prefix.
					_T("PREFIX"), // TODO: 1.98 replaces this with "FONT".
					_T("PUBKEY"), // TODO: What kind of key? Fingerprint?
					_T("DTB"),
				};

				uint32_t type = _little_endian_load_32(&mh->type), size0 = _little_endian_load_32(&mh->size);
				size = size0 - sizeof(*mh);

				_fputts(_T("  - Type: "), stdout);
				if(type < arraysize(types))
					_putts(types[type]);
				else
					_tprintf(_T("%lu\n"), (unsigned long)type);

				_tprintf(_T("    Size: %lu\n"), (unsigned long)size);

				switch(type)
				{
				case OBJ_TYPE_ELF:
					{
						const unsigned char __far *e_ident = (const unsigned char __far *)(mh + 1);

						bool elf_ok = false;

						const Elf32_Shdr *modname = NULL;

						if(size >= EI_NIDENT &&
							e_ident[EI_MAG0] == ELFMAG0 &&
							e_ident[EI_MAG1] == ELFMAG1 &&
							e_ident[EI_MAG2] == ELFMAG2 &&
							e_ident[EI_MAG3] == ELFMAG3 &&
							_one_or_two(e_ident[EI_CLASS]) &&
							_one_or_two(e_ident[EI_DATA]) &&
							e_ident[EI_VERSION] == 1)
						{
#if 0
							_fputts(_T("    Class: "), stdout);
							_putts(e_ident[EI_CLASS] == ELFCLASS32 ? _T("ELF32") : _T("ELF64"));
							_fputts(_T("    Data: "), stdout);
							_putts(e_ident[EI_DATA] == ELFDATA2LSB ? _T("DATA2LSB") : _T("DATA2MSB"));
#endif

							if(e_ident[EI_CLASS] == ELFCLASS32 && e_ident[EI_DATA] == ELFDATA2LSB)
							{
								const Elf32_Ehdr __far *ehdr = (const Elf32_Ehdr __far *)e_ident;

								Elf32_Half type = _little_endian_load_16(&ehdr->e_type);
								Elf32_Half machine = _little_endian_load_16(&ehdr->e_machine);

								Elf32_Word
									shoff = _little_endian_load_32(&ehdr->e_shoff);
								Elf32_Half
									shentsize = _little_endian_load_16(&ehdr->e_shentsize),
									shnum = _little_endian_load_16(&ehdr->e_shnum),
									shstrndx = _little_endian_load_16(&ehdr->e_shstrndx);

#if 0
								_fputts(_T("    Type: "), stdout);
								const _TCHAR *types[] =
								{
									_T("NONE"),
									_T("REL"),
									_T("EXEC"),
									_T("DYN"),
									_T("CORE")
								};

								if(type < arraysize(types))
									_putts(types[type]);
								else
									_tprintf(_T("%#x\n"), type);

								const _TCHAR *machines[] =
								{
									_T("None"),
									_T("WE32100"),
									_T("Sparc"),
									_T("Intel 80386"),
									_T("MC68000"),
									_T("MC88000"),
									_T("Intel MCU"),
									_T("Intel 80860"),
									_T("MIPS R3000"),
									_T("IBM System/370"),
									_T("MIPS R4000 big-endian"),
									_T("Sparc v9 (old)"),
								};

								_fputts(_T("    Machine: "), stdout);
								if(machine < arraysize(machines))
									_putts(machines[machine]);
								else
									_tprintf(_T("%#x\n"), machine);

								printf(_T("    Start of section headers: %lu\n"), (unsigned long)shoff);
								printf(_T("    Size of section headers: %lu\n"), (unsigned long)shentsize);
								printf(_T("    Number of section headers: %lu\n"), (unsigned long)shnum);
								fputs(_T("    Section header string table index: "), stdout);
								if(shstrndx == SHN_UNDEF)
									puts(_T("UNDEF"));
								else
									printf("%lu\n", (unsigned long)shstrndx);
#endif

								if(type == ET_REL &&
									machine == EM_386 &&
									shentsize >= sizeof(Elf32_Shdr) &&
									(uint64_t)shoff + (uint32_t)shentsize * shnum <= size &&
									(shstrndx == SHN_UNDEF || shstrndx < shnum))
								{
									const Elf32_Shdr __far *shdrs = (const Elf32_Shdr __far *)_huge_add(e_ident, shoff);
									if(shstrndx != SHN_UNDEF)
									{
										const Elf32_Shdr __far *strings =
											(const Elf32_Shdr __far *)_huge_add(shdrs, shstrndx * shentsize);

										if(_section_ok(strings, size))
										{
											const Elf32_Shdr __far *shdr = shdrs;

											while(shnum)
											{
												if(_section_ok(shdr, size))
												{
													if(_is_section(e_ident, strings, shdr, ".modname", 9))
														modname = shdr;
												}

												shdr = _huge_add(shdr, shentsize);
												--shnum;
											}
										}
									}

									elf_ok = true;
								}
							}
						}

						print_bool(_T("  ELF OK"), elf_ok);
						if(modname)
						{
							modules[mod].data = e_ident;
							modules[mod].size = size;
							modules[mod].name = e_ident + _little_endian_load_32(&modname->sh_offset);
							
							uint32_t size = _little_endian_load_32(&modname->sh_size);
							const uint8_t *p = modules[mod].name;
							const uint8_t *end = p + size;
							_fputts(_T("    Module name: \""), stdout);
							while(size)
							{
								uint32_t c = _read_utf8(&p, end);
								if(!c)
								{
									--p;
									break;
								}
								print_yaml_char(c);
							}
							_putts("\"");
							
							modules[mod].name_end = p;
						}
					}
					break;
				case OBJ_TYPE_PREFIX:
					{
						_fputts(_T("    Value: "), stdout);
						_puttchar(_T('"'));
						const uint8_t *s = (const uint8_t __huge *)(mh + 1);
						const uint8_t *end = s + size;
						while(s != end)
						{
							uint32_t c = _read_utf8(&s, end);
							if(!c)
								break;
							print_yaml_char(c);
						}
						_putts(_T("\""));
					}
					break;
				}

				++mod;
				mh = _huge_add(mh, size0);
			}

			bool got_mod = false;
			for(mod = 0; mod != mi_modules; ++mod)
			{
				// TODO: Does backslash handling work OK?
				if(modules[mod].name)
				{
					if(!got_mod)
					{
						puts(_T("  B2SUM: |"));
						got_mod = true;
					}

					_fputts(_T("    "), stdout);
					const uint8_t *s = modules[mod].name;
					const uint8_t *end = modules[mod].name_end;
					while(s != end)
					{
						uint8_t c = *s;
						if(c == '\\' || c < 0x20 || c >= 0x7f)
						{
							_puttchar('\\');
							break;
						}
						++s;
					}

					uint8_t out[BLAKE2B_OUTBYTES];
					blake2b512(out, modules[mod].data, modules[mod].size);
					for(unsigned b = 0; b != sizeof(out); ++b)
						_tprintf(_T("%.2x"), out[b]);

					_fputts(_T("  "), stdout);

					s = modules[mod].name;
					while(s != end)
					{
						uint32_t c = *s;
						if(!c)
							break;
						if(c >= 0x20 && c < 0x7f)
							_puttchar(c);
						else if(c == '\n')
							_fputts(_T("\n"), stdout);
						else
							_tprintf(_T("\\%.3o"), c);
						++s;
					}
					_putts(_T(".mod"));
				}
			}

			free(modules);

			_putts(_T("# GRUB code written in C cannot be verified."));
			return EXIT_SUCCESS;
		}
	}

	// kern/i386/pc/startup.S (1.99)
	//   includes:
	//     - rs_decoder.h <- lib/reed_solomon.c
	//     - kern/i386/realmode.S
	//     - kern/i386/pc/lzma_decode.S
	//   offsets:
	//     - total_module_size: .long 0x08
	//     - kernel_image_size: .long 0x0c
	//     - compressed_size: .long 0x010
	//     - install_dos_part: .long 0x14
	//     - install_bsd_part: .long 0x18
	//     - reed_solomon_redundancy: .long 0x1c

	for(i = 0; i != arraysize(grub1_9_kernel); ++i)
	{
		// "GRUB core.img (kernel.img)"
		// TODO.
	}

	print_sector(_T(""), _T("GRUB core.img"), core_sector);
	huge_free(block->mem);
	return EXIT_NO_ID;
}

int check_grub_core(disk_handle *disk, struct _disk_geometry *geometry, void *buffer, uint64_t core_sector)
{
	error_status status = disk_read(disk, (char *)buffer, core_sector, 1);
	if(!error_success(status))
	{
		print_error_yaml(status);
		return EXIT_SYSTEM_ERROR;
	}

	unsigned i = 0;
	for(;;)
	{
		static const _TCHAR class[] = _T("GRUB core.img (diskboot.img)");
		if(i == arraysize(grub2_diskboot))
		{
			print_sector(_T(""), class, core_sector);
			return EXIT_NO_ID;
		}

		if(check_signature(class, &grub2_diskboot[i], core_sector, buffer))
			break;

		++i;
	}

	struct _grub_blocks block;
	int result;

	if(i < GRUB2_DISKBOOT_1_95)
	{
		// Determine the size of the blocklist.
		const struct grub_pc_bios_boot_blocklist_32 *rbegin32 =
			(const struct grub_pc_bios_boot_blocklist_32 *)((const char *)buffer + sector_size) - 1;
		const struct grub_pc_bios_boot_blocklist_32 *rend32 = rbegin32;
		bool rend_ok;

		struct grub_pc_bios_boot_blocklist_64 blocklist_64[512 / sizeof(struct grub_pc_bios_boot_blocklist_32)];
		struct grub_pc_bios_boot_blocklist_64 *rend64 = arrayend(blocklist_64) - 1;

		for(;;)
		{
			if(((const char *)rend32 - (const char *)buffer) < sizeof(*rend32))
			{
				rend_ok = false;
				break;
			}

			rend64->start = rend32->start;
			rend64->len = rend32->len;
			rend64->segment = rend32->segment;

			--rend32;
			--rend64;

			if(!rend32->len)
			{
				rend_ok = true;
				break;
			}
		}

		result = _grub_check_blocklist(&block, disk, arrayend(blocklist_64) - 1, rend64, rend_ok);
	}
	else
	{
		// Determine the size of the blocklist.
		const struct grub_pc_bios_boot_blocklist_64 *rbegin =
			(const struct grub_pc_bios_boot_blocklist_64 *)((const char *)buffer + sector_size) - 1;
		const struct grub_pc_bios_boot_blocklist_64 *rend = rbegin;
		bool rend_ok;

		for(;;)
		{
			if(((const char *)rend - (const char *)buffer) < sizeof(*rend))
			{
				rend_ok = false;
				break;
			}

			--rend;

			if(!rend->len)
			{
				rend_ok = true;
				break;
			}
		}

		result = _grub_check_blocklist(&block, disk, rbegin, rend, rend_ok);
	}

	if(result)
		return result;

	core_sector += 1;

	block.data = (const unsigned char __huge *)block.mem + 512;

	for(;;)
	{
		int result = _check_grub_kernel(&block, core_sector);
		if(result >= 0)
			return result;
		core_sector = ~0;
	}

	// huge_free(block.data);

	// TODO: the compressed image...
	// ...which starts with startup.S
	// ...and ends with a set of grub_module_infos and grub_module_headers (look for 'mimg') with pointers to prefix and config.
	// The 'gmim' is at the end of the kernel. It knows its own size. Fetch _edata from the startup.S code.
	// return EXIT_NO_ID;
}
