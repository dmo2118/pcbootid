/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "pcbootid.h"
#include "check.h"

static void _print_stderr(const _TCHAR *s)
{
	_fputts(s, stderr);
}

static void _print_error_stderr(const _TCHAR *prefix, error_status error)
{
	_fputts(prefix, stderr);
	_fputts(_T(": "), stderr);
	print_error(error, _print_stderr);
	_puttc(_T('\n'), stderr);
}

static int _usage(_TCHAR **argv)
{
	_ftprintf(stderr, _T("Usage: %s [-h heads] [-s sectors] [-g core.img sector] disk\n"), argv[0]);
	return EXIT_USAGE;
}

static unsigned long _int_arg(_TCHAR **argv)
{
	_TCHAR *endptr;
	unsigned long result = _tcstoul(optarg, &endptr, 0);
	if(*endptr || result >= 0x10000 || !result)
	{
		_usage(argv);
		return 0;
	}
	return result;
}

int _tmain(int argc, _TCHAR **argv)
{
	_TCHAR *disk_path;
	disk_handle disk;
	struct _disk_geometry geometry = {0};
	int (*check)(disk_handle *, struct _disk_geometry *, void *, uint64_t) = check_boot_record;
	uint64_t sector_offset = 0;

	union
	{
		uint8_t b[sector_size * 13]; // TODO: MAX_SECTORS safety check in _bpb_read_more

		/*
		From Ralf Brown's Interrupt List, INT 13/AH=02h, DISK - READ SECTOR(S) INTO MEMORY:
		BUGS:   When reading from floppies, some AMI BIOSes (around 1990-1991) trash
		          the byte following the data buffer, if it is not arranged to an even
		          memory boundary.  A workaround is to either make the buffer word
		          aligned (which may also help to speed up things), or to add a dummy
		          byte after the buffer.
		*/
		void *align;
	} buffer;

	/* TODO: Use -h and -s repeatedly. */

	for(;;)
	{
		int c = getopt(argc, argv, _T("h:s:g:"));
		if(c < 0)
			break;
		switch(c)
		{
		case 'h':
			geometry.heads = _int_arg(argv);
			if(!geometry.heads)
				return EXIT_USAGE;
			break;
		case 's':
			geometry.sectors = _int_arg(argv);
			if(!geometry.sectors)
				return EXIT_USAGE;
			break;
		case 'g':
			check = check_grub_core;
			{
				_TCHAR *endptr;
				sector_offset = _tcstoul(optarg, &endptr, 0);
				if(*endptr)
					return _usage(argv);
			}
			break;
		default:
			return _usage(argv);
		}
	}

	int result = EXIT_SUCCESS;

	for(;;)
	{
		disk_path = argv[optind];
		if(!disk_path)
			break;

		disk = disk_open(disk_path);
		if(!disk_open_success(&disk))
		{
			_print_error_stderr(disk_path, last_error());
			result = EXIT_SYSTEM_ERROR;
		}
		else
		{
			_tprintf(_T("--- # %s\n"), disk_path);

			int next_result = check(&disk, &geometry, &buffer, sector_offset);
			if(next_result)
				result = next_result;

			disk_close(&disk);
		}

		++optind;
	}

	return result;
}
