/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef OS_H
#define OS_H

#include "pcbootid.h"

struct _disk_geometry
{
	unsigned long heads, sectors;
};

typedef void (*print_error_func)(const _TCHAR *str);

#if __unix__

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>	

#if __linux__
#	include <linux/hdreg.h>
#elif __CYGWIN__
#	include <cygwin/hdreg.h>
#endif

enum
{
	error_no_memory = ENOMEM,
	error_buffer_overflow = EIO // TODO: Better errors.
};

typedef int error_status;
typedef int disk_handle;

static inline void print_error(error_status error, print_error_func print)
{
	print(strerror(error));
}

static inline error_status last_error()
{
	return errno;
}

static inline int error_success(error_status status)
{
	return !status;
}

static inline disk_handle disk_open(const _TCHAR *path)
{
	return open(path, O_RDONLY);
}

static inline bool disk_open_success(const disk_handle *disk)
{
	return *disk >= 0;
}

static inline void disk_close(disk_handle *disk)
{
	close(*disk);
}

static inline bool disk_get_geometry(const disk_handle *disk, struct _disk_geometry *geometry)
{
	struct hd_geometry geometry_raw;

	// TODO: HDIO_GETGEO_long?
	// TODO: This actually working?
	if(ioctl(*disk, HDIO_GETGEO, &geometry_raw) < 0)
		return false;

	geometry->heads = geometry_raw.heads;
	geometry->sectors = geometry_raw.sectors;
	return true;
}

#elif _WIN32

#include <windows.h>

enum
{
	error_no_memory = ERROR_NOT_ENOUGH_MEMORY,
	error_buffer_overflow = ERROR_BUFFER_OVERFLOW
};

typedef DWORD error_status;
typedef HANDLE disk_handle;

static inline error_status last_error()
{
	return GetLastError();
}

static inline disk_handle disk_open(const _TCHAR *path)
{
	return CreateFile(path, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
}

static inline bool error_success(error_status error)
{
	return !error;
}

static inline bool disk_open_success(const disk_handle *disk)
{
	return *disk != INVALID_HANDLE_VALUE;
}

static inline void disk_close(disk_handle *d)
{
	CloseHandle(*d);
}

static inline bool disk_get_geometry(const disk_handle *disk, struct _disk_geometry *geometry)
{
	DWORD bytes_returned;
	DISK_GEOMETRY geometry_raw;
	if(
		!DeviceIoControl(
			*disk, 
			IOCTL_DISK_GET_DRIVE_GEOMETRY, 
			NULL, 
			0, 
			&geometry_raw, 
			sizeof(geometry_raw), 
			&bytes_returned, 
			NULL) ||
		bytes_returned != sizeof(geometry_raw))
	{
		return false;
	}

	geometry->heads = geometry_raw.TracksPerCylinder;
	geometry->sectors = geometry_raw.SectorsPerTrack;
	return true;
}

#elif MSDOS

// TODO: Drive locking for Windows 95.

#include <i86.h>

enum
{
	_api_invalid = -1,
	_api_int13,
	_api_int13e,
};

enum
{
	error_no_memory = 0xbb // TODO: This sucks. Get a better error code.
};

typedef uint8_t error_status;

typedef error_status (*_read_func)(const struct disk_handle *disk, void *buffer, uint64_t offset, unsigned size);

typedef struct disk_handle
{
	uint8_t _drive;
	uint8_t _sectors;
	uint16_t _heads;
	_read_func _read;
} disk_handle;

typedef const disk_handle *disk_geometry;

static inline error_status last_error()
{
	/* We only ever get here if the drive letter couldn't be looked up */
	return 0xaa; /* Drive not ready */
}

inline bool error_success(error_status error)
{
	return !error;
}

/*
From Intel® 64 and IA-32 Architectures Optimization Reference Manual

Assembly/Compiler Coding Rule 37. (M impact, MH generality) Break
dependences on portions of registers between instructions by operating on 32-bit
registers instead of partial registers. For moves, this can be accomplished with 32-
bit moves or by using MOVZX.

x86-16 unavoidably uses partial registers when running on a 32/64-bit processor.

16-bit Intel processors did not use intruction-level parallelism.
*/

struct _dos_ddt
{
	const struct _dos_ddt __far *next;
	uint8_t driveno;
	uint8_t logdriveno;
};

const struct _dos_ddt __far *_dos_get_ddt_list();

/* TODO: Read three times for INT 13? */

/* Skip the installation check. (INT 21/AX=0800) */
/* Requires DEVICE.SYS prior to DOS 4.0. */
#pragma aux _dos_get_ddt_list = \
	"	mov di, 0ffffh" /* If not installed. */ \
	"	mov cx, ds" /* Save DS */ \
	"	mov ax, 0803h" \
	"	int 2fh" \
	"	mov ax, ds" \
	"	mov es, ax" \
	"	mov ds, cx" \
	value [es di] /* [ds di] doesn't work. */ \
	modify [ax cx];

uint8_t _int13e_test(uint8_t drive);

#pragma aux _int13e_test = \
	/* INT 13/AH=41h */ \
	"	mov ah, 41h" \
	"	mov bx, 55aah" \
	"	int 13h" \
	"	jc no_int13e" \
	"	cmp bx, 0aa55h" \
	"	jne no_int13e" \
	"	and cl, 1" \
	"	jnz done" \
	"no_int13e:" \
	"	xor cl, cl" \
	"done:" \
	parm [dl] \
	value [cl] \
	modify [ah bx dx];

void _get_drive_parameters(uint8_t drive, disk_handle *result);

#pragma aux _get_drive_parameters = \
	"	mov bl, dl" /* Save DL */ \
	"	mov ah, 15h" \
	"	int 13h" \
	"	jc no_drive" \
	"	cmp ax, 3h" /* SpeedStor */ \
	"	je has_drive0" \
	"	test ah, ah" \
	"	jz no_drive" \
	"has_drive0:" \
	/* Sometimes this returns success even though it failed. (Compare against DL.) */ \
	/* And also, sometimes DL is wrong. (So check AH=15h.) */ \
	/* INT 13/AH=08h GET DRIVE PARAMETERS */ \
	"	mov dl, bl" /* Restore DL */ \
	"	mov ah, 08h" \
	"	xor di, di" \
	"	mov es, di" \
	"	int 13h" \
	"	jnc has_drive1" \
	"no_drive:" \
	"	mov [si+1], 0" \
	"	jmp done" \
	"has_drive1:" \
	/* Heads */ \
	"	mov dl, dh" \
	"	xor dh, dh" \
	"	inc dx" \
	"	mov [si+2], dx" \
	/* Sectors */ \
	"	and cl, 3fh" \
	"write_sector:" \
	"	mov [si+1], cl" \
	"done:" \
	parm [dl] [si] \
	modify [ax bl cx dx es di];

static error_status _read_int13(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size);
static error_status _read_int13e(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size);

static inline disk_handle disk_open(const _TCHAR *path)
{
	// 0x##, ##h -> INT 13(e)
	// X:\ -> INT
	static const disk_handle failure = {0};
	static const _read_func read_funcs[] = {_read_int13, _read_int13e};
	disk_handle result;

	if((path[0] >= 'a' && path[0] <= 'z' || path[0] >= 'A' && path[0] <= 'Z') && !path[1] || (path[1] == ':' && (!path[2] || path[2] == '\\' && !path[3])))
	{
		const struct _dos_ddt __far *ddt = _dos_get_ddt_list();
		char logdriveno = path[0];

		if(logdriveno >= 'a' && logdriveno <= 'z')
		{
			logdriveno -= 'a';
		}
		else
		{
			assert(logdriveno >= 'A' && logdriveno <= 'Z');
			logdriveno -= 'A';
		}

		/*
		Alternatives:
		- Read the disk with INT 21/AX=440Dh/CX=0861h
		  - Limited size
		- Get the INT 13 unit with INT 21/AX=440Dh/CX=086Fh
		  - MS-DOS 7+, not on FreeDOS.
		- Follow the directions in Q62571.
		  - Article doesn't cover drive letter assignment for hard drives.
		    - Wikipedia does, but leaves out edge cases like DoubleSpace/DriveSpace.

		Not alternatives:
		- INT 25h "Absolute Disk Read" (or its Windows 95 replacement, INT 21/AX=7305h)
		  - Only reads partitions.
		- INT 21/AH=32h "Get DPB" (or its Windows 95 replacement, INT 21/AX=7302h)
		  - Includes a "unit" that isn't an INT 13 drive.
		*/

		for(;;)
		{
			if(FP_OFF(ddt) == 0xffff)
				return failure;

			if(ddt->logdriveno == logdriveno)
				break;

			ddt = ddt->next;
		}

		result._drive = ddt->driveno;
	}
	else
	{
		bool zero_x = path[0] == '0' && path[1] == 'x';
		unsigned long drive;
		char *endstr;

		if(zero_x)
			path += 2;

		drive = strtoul(path, &endstr, 16);

		if(!zero_x && *endstr == 'h')
			++endstr;
		if(*endstr || drive >= 0x100)
			return failure;

		result._drive = drive;
	}

	result._read = read_funcs[_int13e_test(result._drive)];

	/* Always try to get the geometry here. But if the geometry isn't available, don't assume INT 13E isn't either. */
	_get_drive_parameters(result._drive, &result);

	/* printf("%d %d\n", (unsigned)result._heads, (unsigned)result._sectors); */
	
	return result;
}

static inline bool disk_open_success(const disk_handle *disk)
{
	return disk->_read != NULL;
}

static inline void disk_close(disk_handle *disk)
{
}

static inline bool disk_get_geometry(const disk_handle *disk, struct _disk_geometry *geometry)
{
	geometry->heads = disk->_heads;
	geometry->sectors = disk->_sectors;
	return disk->_sectors;
}

static inline error_status disk_read(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size)
{
	return disk->_read(disk, buffer, offset, size);
}

#endif

#if __unix__

#include <unistd.h>

#else

extern _TCHAR *optarg;
extern int optind;

/* DOS/Windows-appropriate behavior. */
int getopt(int argc, _TCHAR *const *argv, const _TCHAR *optstring);

#endif

void print_error(error_status error, print_error_func print);
error_status disk_read(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size);

#endif
