/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "check.h"
#include "hashes.h"
#include "print.h"

struct _chs
{
	uint8_t head;
	uint8_t sector_cylinder_hi;
	uint8_t cylinder_lo;
};

static inline uint16_t _chs_cylinder(const struct _chs *chs)
{
	return chs->cylinder_lo | ((uint16_t)(chs->sector_cylinder_hi & 0xc) << 2);
}

static inline uint8_t _chs_sector(const struct _chs *chs)
{
	return chs->sector_cylinder_hi & 0x3f;
}

struct _mbr_partition
{
	uint8_t status;
	struct _chs first_sector;
	uint8_t type;
	struct _chs last_sector;
	uint32_t lba;
	uint32_t sectors;
};

static_assert(sizeof(struct _mbr_partition) == 16, "Bad _mbr_partition size");

static bool _partition_table(
	const struct _disk_geometry *geometry, 
	const void *mbr, 
	uint64_t *sector_offset, 
	unsigned offset, 
	unsigned count)
{
	const struct _mbr_partition *partitions = (const void *)((const char *)mbr + offset);
	unsigned i;
	const struct _mbr_partition *partition = NULL;
	uint16_t cylinder;
	uint8_t sector;
	bool multiple_active_partitions = false;

	for(i = 0; i != count; ++i)
	{
		if(partitions[i].status & 0x80)
		{
			if(partition)
				multiple_active_partitions = true;
			else
				partition = &partitions[i];
		}
	}

	print_bool(_T("Active partitions <= 1 OK"), !multiple_active_partitions);

	_fputts(_T("  Active partition: "), stdout);

	if(!partition)
	{
		_puttchar(_T('\n'));
		return false;
	}

	_tprintf(_T("%lu\n"), (unsigned long)(partition - partitions) + 1);

	*sector_offset = _little_endian_load_32(&partition->lba);
	cylinder = _chs_cylinder(&partition->first_sector);
	sector = _chs_sector(&partition->first_sector);
	if(!(cylinder == 1023 &&
		(partition->first_sector.head == 254 || (partition->type == 0xee && partition->first_sector.head == 255)) &&
		sector == 63))
	{
		if(!geometry->heads || !geometry->sectors)
		{
			if(!*sector_offset)
			{
				_putts(_T("  - no disk geometry"));
				return false;
			}
		}
		else
		{
			uint64_t chs = ((uint64_t)cylinder * geometry->heads + partition->first_sector.head) * geometry->sectors + sector - 1;
			if(*sector_offset)
				print_bool(_T("CHS = LBA OK"), chs == *sector_offset);
			if(!*sector_offset)
				*sector_offset = chs;
		}
	}

	return true;
}

static bool _partition_table_4(const struct _disk_geometry *geometry, const void *mbr, uint64_t *sector_offset)
{
	return _partition_table(geometry, mbr, sector_offset, 0x1be, 4);
}

static void _print_files()
{
	_putts(_T("- Class: File\n  Files:"));
}

static void _check_55aa(const void *sector)
{
	/* Anything getting loaded at 007C:0000 should have this signature. */
	print_bool(_T("55AA signature OK"), _little_endian_load_16((const uint8_t *)sector + 0x1fe) == 0xaa55);
}

static void _fail_bios_id(uint64_t sector_offset, const void *sector)
{
	print_sector(_T("?"), NULL, sector_offset);
	_check_55aa(sector);
}

static void _print_trimmed(const char *s, unsigned length)
{
	while(length && s[length - 1] == ' ')
		--length;

	while(length)
	{
		print_yaml_char_oem(*s);
		++s;
		--length;
	}
}

static void _print_8_3(const void *sector, unsigned offset)
{
	const char *s = ((const char *)sector) + offset;
	_fputts(_T("  - \""), stdout);
	_print_trimmed(s, 8);

	if(s[8] != ' ' || s[9] != ' ' || s[10] != ' ')
	{
		_puttchar(_T('.'));
		_print_trimmed(s + 8, 3);
	}

	_putts(_T("\""));
}

static bool _is_zero(const void *buffer, unsigned start, unsigned end, uint64_t sector_offset)
{
	const uint8_t *b8 = buffer;
	for(unsigned i = start; i != end; ++i)
	{
		if(b8[i])
			return false;
	}

	print_sector(_T("No boot code"), NULL, sector_offset);
	_check_55aa(buffer);
	return true;
}

static bool _bpb_read(disk_handle *disk, void *buffer, uint64_t offset, unsigned size)
{
	error_status status = disk_read(disk, buffer, offset, size);
	if(!error_success(status))
	{
		print_error_yaml(status);
		return false;
	}

	return true;
}

static bool _bpb_read_more(disk_handle *disk, uint64_t sector_offset, void *sector, unsigned size)
{
	return _bpb_read(disk, (char *)sector + sector_size, sector_offset + 1, size - 1);
}

static bool _bpb_read_one(disk_handle *disk, uint64_t sector_offset, void *sector, unsigned offset)
{
	return _bpb_read(disk, (char *)sector + offset * sector_size, sector_offset + offset, 1);
}

static void _bpb_check(disk_handle *disk, uint64_t sector_offset, void *sector, unsigned jmp_offset)
{
	if(_is_zero(sector, jmp_offset, sector_size - 2, sector_offset))
		return;

	// TODO: Are IO.SYS/MSDOS.SYS allowed to be fragmented?
	// TODO: Check the root directory for IO.SYS/MSDOS.SYS in the right directory entries.

	for(unsigned i = 0; i != arraysize(vbr); ++i)
	{
		if(jmp_offset == signature_begin(&vbr[i].base) && check_signature(_T("VBR"), &vbr[i], sector_offset, sector))
		{
			const _TCHAR *filesystem;

			_check_55aa(sector);

			_fputts(_T("  Filesystem: "), stdout);
			if(vbr[i].flags & FLAG_FAT)
				filesystem = _T("FAT");
			else if(vbr[i].flags & FLAG_FAT32)
				filesystem = _T("FAT32");
			else if(vbr[i].flags & FLAG_EXFAT)
				filesystem = _T("exFAT");
			else if(vbr[i].flags & FLAG_NTFS)
				filesystem = _T("NTFS");
			else
				filesystem = _T("");
			_putts(filesystem);

			switch(i)
			{
			case VBR_DOS_FAT:
				_print_files();
				_print_8_3(sector, 0x1e6);
				_print_8_3(sector, 0x1f1);
				break;

			case VBR_WINDOWS_95_FAT32:
				if(!_bpb_read_one(disk, sector_offset, sector, 2))
					return;
				print_bool(_T("Logical sector 2 OK"), check_signature_base(&windows_95_fat32_vbr, sector));
				_print_files();
				_print_8_3(sector, 0x1d8);
				// TODO: Are MSDOS.SYS and WINBOOT.SYS used?
				_print_8_3(sector, 0x1e3);
				_print_8_3(sector, 0x1f1);
				break;

			case VBR_WINDOWS_98_FAT:
				_print_files();
				_print_8_3(sector, 0x1d8);
				_print_8_3(sector, 0x1e3);
				break;

			case VBR_WINDOWS_2000_FAT:
				_print_files();
				_print_8_3(sector, 0x1a1);
				break;

			case VBR_WINDOWS_2000_FAT32:
				if(!_bpb_read_one(disk, sector_offset, sector, 12))
					return;
				print_bool(_T("Logical sector 12 OK"), check_signature_base(&windows_2000_fat32_vbr, sector));
				// TODO: Check for possible string loops with 0xff.
				// TODO: Disassemble extra code.
				_print_files();
				_print_8_3(sector, 0x170);
				break;

			case VBR_WINDOWS_2000_NTFS:
				if(!_bpb_read_more(disk, sector_offset, sector, 7))
					return;

				{
					const _TCHAR *version;
					/* TODO: windows_2000_sp0_ntfs is untrusted. */

					_fputts(_T("  Version: "), stdout);
					if(check_signature_base(&windows_2000_sp0_ntfs_vbr, sector))
						version = _T("RTM");
					else if(check_signature_base(&windows_2000_ntfs_vbr, sector))
						version = _T("KB320397");
					else
						version = _T("");
					_putts(version);
				}

				_print_files();
				_putts(_T("  - \"NTLDR\""));
				break;

			case VBR_WINDOWS_XP_EXFAT:
				// TODO: This doesn't boot. Return as necessary.
				break;

			case VBR_WINDOWS_VISTA_FAT32:
				if(!_bpb_read_one(disk, sector_offset, sector, 12))
					return;
				print_bool(_T("Logical sector 12 OK"), check_signature_base(&windows_vista_fat32_vbr, sector));
				_print_files();
				_print_8_3(sector, 0x169);
				break;

			case VBR_WINDOWS_VISTA_NTFS:
				if(!_bpb_read_more(disk, sector_offset, sector, 9))
					return;
				// TODO: $Boot unverified.
				print_bool(_T("$Boot OK"), check_signature_base(&windows_vista_ntfs_vbr, sector));
				_print_files();
				_putts(_T("  - \"BOOTMGR\"\n  - \"NTLDR\""));
				break;

			case VBR_WINDOWS_7_NTFS:
				if(!_bpb_read_more(disk, sector_offset, sector, 9))
					return;
				print_bool(_T("$Boot OK"), check_signature_base(&windows_7_ntfs_vbr, sector));

				// TODO: Grab $Boot from Starman.
				_print_files();
				_putts(_T("  - \"BOOTMGR\"\n  - \"NTLDR\""));

				// TODO: BOOTMGR

				// TODO: strings
				// TODO: Remainder of $Boot
				break;

			case VBR_WINDOWS_8_FAT32:
				if(!_bpb_read_one(disk, sector_offset, sector, 12))
					return;
				strings_key();
				str16_valid(sector, 0x1f8, 0, true);
				str16_valid(sector, 0x1fa, 0, true);
				str_valid(sector + 0x1800, 0, true);

				print_bool(_T("Logical sector 12 OK"), check_signature_base(&windows_8_fat32_vbr, sector));
				_print_files();
				_print_8_3(sector, 0x16d);
				break;

			case VBR_WINDOWS_8_EXFAT:
				if(!_bpb_read_more(disk, sector_offset, sector, 3))
					return;
				print_bool(_T("Extended boot sectors OK"), check_signature_base(&windows_8_exfat_vbr, sector));
				_print_files();
				_putts("  - \"BOOTMGR\"");
				break;

			// case VBR_WINDOWS_8_NTFS:
			// TODO: Remainder of $Boot
			// TODO: Files
			}

			return;
		}
	}

	for(unsigned i = 0; i != arraysize(grub2_boot); ++i)
	{
		// TODO: For all GRUB:
		// * Check jump/NOPs.
		// * With/without HYBRID_BOOT. (2.02, Debian 2.02-1)

		const struct signature_grub2_boot *sig = &grub2_boot[i];

		// unsigned signature_begin = signature_begin(&sig->full.base);

		if(jmp_offset == sig->after_bpb && check_signature(_T("GRUB boot.img"), &sig->full, sector_offset, sector))
		{
			// TODO: GRUB 1.91+: Check the jump/NOPs after the CLI.
			_fputts(_T("  Disk: "), stdout);
			_putts(check_signature_base(&sig->floppy, sector) ? _T("floppy"): _T("fixed"));

			// TODO: Next step: JMP to 0000:[0x5a] in sector at 0x5c, in diskboot.S. This loads the rest of core.img, then
			// startup_raw.S.

			uint8_t *boot_drive_check = (uint8_t *)sector + sig->after_bpb + 1;
			bool boot_drive_check_ok = true;
			_fputts(_T("  Boot drive check: "), stdout);
			if(boot_drive_check[0] == 0xeb && boot_drive_check[1] == 0x05) // jmp 3f
				_putts(_T("false"));
			else if(boot_drive_check[0] == 0x90 && boot_drive_check[1] == 0x90) // nop; nop
				_putts(_T("true"));
			else
			{
				_puttchar(_T('\n'));
				boot_drive_check_ok = false;
			}

			print_bool(_T("Boot drive check OK"), boot_drive_check_ok);

			const void *core_sector_ptr = (const uint8_t *)((const char *)sector + sig->core_sector);
			uint64_t core_sector;
			core_sector = i >= GRUB2_BOOT_1_95 ?
				_little_endian_load_64(core_sector_ptr) :
				_little_endian_load_32(core_sector_ptr);

			uint8_t boot_drive = *(const uint8_t *)((const char *)sector + sig->boot_drive);
			_fputts(_T("  INT 13H boot drive: "), stdout);

			if(boot_drive == 0xff)
			{
				_puttchar(_T('\n'));
				check_grub_core(disk, NULL, sector, core_sector);
			}
			else
			{
				_tprintf(_T("%#.2x\n"), (unsigned)boot_drive);
				_tprintf(_T("# Continue with `pcbootid -g %llu [INT 13H boot drive]`\n"), (unsigned long long)core_sector);
			}

			return;
		}

		/* TODO: Is 1.99~beta0 worth bothering with? */
	}

	for(unsigned i = 0; i != arraysize(pi); ++i)
	{
		const struct signature_full *s = &pi[i];
		if(jmp_offset < sector_size - signature_end(&s->base) &&
			check_signature(NULL, s, sector_offset, (const char *)sector + jmp_offset))
		{
			strings_key();
			str16_valid(sector, jmp_offset + 3, 0x7c00, false);
			return;
		}
	}

	_fail_bios_id(sector_offset, sector);
}

int check_boot_record(disk_handle *disk, struct _disk_geometry *geometry, void *buffer, uint64_t sector_offset)
{
	if(!geometry->heads || !geometry->sectors)
	{
		struct _disk_geometry disk_geometry;
		if(!disk_get_geometry(disk, &disk_geometry))
		{
			_putts(_T("# Not a disk. Use -h and -s to check CHS geometry."));
		}
		else
		{
			if(!geometry->heads)
				geometry->heads = disk_geometry.heads;
			if(!geometry->sectors)
				geometry->sectors = disk_geometry.sectors;
		}
	}

	/* printf("Geometry: %d/%d\n", disk_geometry_heads(&geometry), disk_geometry_sectors(&geometry)); */

	for(;;)
	{
		error_status status = disk_read(disk, buffer, sector_offset, 1);
		if(!error_success(status))
		{
			print_error_yaml(status);
			return EXIT_SYSTEM_ERROR;
		}

		// _debug_hex(buffer);

		// TODO: Coverage testing for INT 13 code. At least test non-extended version. Error handling on read might be wrong?
		// TODO: INT 13: Repeat when disk changed

		if(_is_zero(buffer, 0, 0xda, sector_offset))
			return EXIT_SUCCESS;

		unsigned i = 0;
		for(;;)
		{
			if(i == arraysize(mbr))
			{
				uint8_t *b = buffer;
				if(b[0] == 0xeb && b[1] < 0x80)
					_bpb_check(disk, sector_offset, buffer, b[1] + 2);
				else if(b[0] == 0xe9)
					_bpb_check(disk, sector_offset, buffer, _little_endian_load_16(b + 1) + 3);
				else if(_is_zero(buffer, 0, 3, sector_offset))
					return EXIT_SUCCESS;
				else
					_fail_bios_id(sector_offset, buffer);

				return EXIT_NO_ID;
			}

			if(check_signature(_T("MBR"), &mbr[i], sector_offset, buffer))
			{
				_check_55aa(buffer);

				switch(i)
				{
				/* DOS_MBR: Invalid partition table: partition status not in [0x00, 0x80] */

				case MBR_WINDOWS_95:
					strings_key();
					str16_valid(buffer, 0x3c, 0x600 + 1, false);
					str16_valid(buffer, 0xbd, 0x600, false);
					str16_valid(buffer, 0xa9, 0x600, false);
					break;

				/* TODO, HP_COMPAQ_MBR: Handle the funky fallback behavior here. */

				case MBR_PARTITION_WIZARD:
					_fputts("  ID: \"", stdout);

					{
						const char *b = buffer;
						size_t i = 0xff;
						while(b[i] && i != 0x440)
						{
							print_yaml_char_oem(b[i]);
							++i;
						}
					}

					_fputts("\"\n", stdout);
					break;
				}

				if(!_partition_table_4(geometry, buffer, &sector_offset))
					return EXIT_BOOT_CRASH;

				break;
			}

			++i;
		}
	}
}
