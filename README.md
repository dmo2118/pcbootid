pcbootid
========

Identifies the boot code present on a hard disk or floppy that would be used to boot an IBM-compatible PC. `pcbootid` checks
code and data structures found in master boot records, HDD track 0, boot sectors, and the like, stopping once control of the
system is handed off to code located in ordinary files.

`pcbootid` currently recognizes boot code from many versions of GRUB 2, Windows, and DOS.

Many boolean fields output by `pcbootid` have `OK` in the name. If any of these fields are `false`, then the system may not
boot. Empty fields (a `null` in YAML) may also result in boot failure.

`pcbootid` outputs [YAML](https://yaml.org/). At this stage, the values in fields output by `pcbootid` should not change their
meanings, though in the future fields may be added or removed.

Later versions of `pcbootid` may fail to identify boot records that earlier versions recognize, due to more stringent tests.

El Torito bootable CD-ROMs are not supported. UEFI is not supported.

Building
--------

### Dependencies

* A POSIX build environment, such as GNU/Linux or Cygwin.
* [Automake](https://www.gnu.org/software/automake/) and [Autoconf](https://www.gnu.org/software/autoconf/)
* `elf.h`, typically provided by the [GNU C Library](https://www.gnu.org/software/libc/)
* [liblzma](https://tukaani.org/xz/)
* [OpenSSL](https://www.openssl.org/), 1.1.0 or greater
* [Python 3](https://www.python.org/)

### And then

```
aclocal -I.
autoconf
./configure
make
```

Building `hashes.h` and `hashes.c` requires `python3`. These two files are identical on every platform, and can be safely copied
from any machine with `python3` to one that does not.

Security
--------

`pcbootid` attempts to handle many edge cases, but many others still need to be filled in. Consequently, `pcbootid` should not
be used in situations where security is a concern.

License
-------

Different parts of `pcbootid` are under different licenses. These are as follows:

pcbootid: All files, with the exception of:

* `image` (and its contents)
* `apache20.txt`
* `ax_check_compile_flag.m4`
* `hp_compaq.s`
* `gpl-3.0.md`
* `md32_common.h`
* `sha.h`
* `sha256.c`
* Any other file if and only if a copyright is specified in its contents.

> Copyright (C) 2019  David Odell <dmo2118@gmail.com>
>
> This program is free software: you can redistribute it and/or modify
> it under the terms of the [GNU General Public License](gpl-3.0.md) as published by
> the Free Software Foundation, version 3 of the License.
>
> This program is distributed in the hope that it will be useful,
> but WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
> GNU General Public License for more details.
>
> You should have received a copy of the GNU General Public License
> along with this program.  If not, see <https://www.gnu.org/licenses/>.

`ax_check_compile_flag.m4`:

> Copyright (c) 2008 Guido U. Draheim <guidod@gmx.de>
> Copyright (c) 2011 Maarten Bosmans <mkbosmans@gmail.com>
>
> This program is free software: you can redistribute it and/or modify it
> under the terms of the GNU General Public License as published by the
> Free Software Foundation, either version 3 of the License, or (at your
> option) any later version.
>
> This program is distributed in the hope that it will be useful, but
> WITHOUT ANY WARRANTY; without even the implied warranty of
> MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
> Public License for more details.
>
> You should have received a copy of the GNU General Public License along
> with this program. If not, see <https://www.gnu.org/licenses/>.
>
> As a special exception, the respective Autoconf Macro's copyright owner
> gives unlimited permission to copy, distribute and modify the configure
> scripts that are the output of Autoconf when processing the Macro. You
> need not follow the terms of the GNU General Public License when using
> or distributing such scripts, even though portions of the text of the
> Macro appear in them. The GNU General Public License (GPL) does govern
> all other use of the material that constitutes the Autoconf Macro.
>
> This special exception to the GPL applies to versions of the Autoconf
> Macro released by the Autoconf Archive. When you make and distribute a
> modified version of the Autoconf Macro, you may extend this special
> exception to the GPL to apply to your modified version as well.

`hp_compaq.s`:

> To the extent possible under law, David Odell <dmo2118@gmail.com> has waived all copyright and related or neighboring
> rights to hp_compaq.s. This work is published from: United States.
>
> CC0 1.0 Universal applies to this work <cc0.xhtml>; other parties may have rights to a portion of this work.

Files in the `image` directory are copyright their respective owners. Refer to `hashes.json` for a list of sources.

`md32_common.h`, `sha.h`, and `sha256.c` are modified versions of code found in [OpenSSL](https://www.openssl.org/).

> The OpenSSL toolkit is licensed under the [Apache License 2.0](apache20.txt), which means
> that you are free to get and use it for commercial and non-commercial
> purposes as long as you fulfill its conditions.

`gpl-3.0.md`: The GNU General Public License, Version 3

> Copyright (C) 2007 Free Software Foundation, Inc. <https://fsf.org/>
>
> Everyone is permitted to copy and distribute verbatim copies of this
> license document, but changing it is not allowed.
