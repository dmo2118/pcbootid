	# To the extent possible under law, David Odell <dmo2118@gmail.com> has waived all copyright and related or neighboring
	# rights to hp_compaq.s. This work is published from: United States.

	# CC0 1.0 Universal applies to this work <cc0.xhtml>; other parties may have rights to a portion of this work.

	# While HP/Compaq is a likely candidate for the authorship of this boot record, there are no identifying marks which state
	# beyond a doubt that this is the case. I have no indication that this is part of a larger software package; this boot code
	# is perfectly functional with any industry-standard BIOS, requiring nothing else.

	# Pulled from a hard drive shipped with a Compaq computer from 2007.

	# ./as.sh hp_compaq.s 0

	.include "bootrec.s"

	{store} xor di,di
	mov si,0x200
	mov ss,di
	mov sp,0x7a00
	mov bx,0x7a0
	{store} mov cx,si
	mov ds,bx
	mov es,bx
	# MBRs usually have ES:DI = 0000:0600, and SS:SP = 0000:7C00.
	# SS:SP = 0000:7A00, BX = 0x7A0
	rep movsb                   # memcpy(ES:DI = 07A0:0000, DS:SI = 07A0:0200, CX = 0x0200)
	jmp 0x7a0:real_start

int13_disk_address_packet:
	.byte 0x10                  # size of packet (10h or 18h)
	.byte 0x00                  # reserved (0)
	.short 0x01                 # number of blocks to transfer (max 007Fh for Phoenix EDD)

dap_transfer_buffer:
	.set dap_transfer_buffer_byte, dap_transfer_buffer + 1
	.long 0x00007A00            # transfer buffer

dap_start_lba:
	.8byte 0x0000000000000000   # starting absolute block number

new_partition_type:
	.byte PARTITION_TYPE_NTFS

fn_read_boot_record_partition:  # DI: pointer to partition record
	mov edx,[di+PARTITION_LBA_START]
fn_read_boot_record:            # EDX: LBA I/O start
	mov ah,INT13_EXTENDED_READ
	mov byte ptr [dap_transfer_buffer_byte],0x7c
fn_io_boot_record:              # AH: INT 13 function
	{store} xor al,al           # write flags (read: ignored)
	mov [dap_start_lba],edx
	mov si,OFFSET FLAT:int13_disk_address_packet
	mov dl,0x80
	int 0x13
	jc io_error
	cmp word ptr [0x3fe],0xaa55
ret:
	ret

fn_message:                     # DS:SI: ASCIIZ string
	lodsb                       # AL = character to write
	{store} or al,al
	jz ret
	mov ah,VIDEO_TELETYPE_OUTPUT
	mov bx,0x7                  # BH = page number, BL = foreground color (graphics modes only)
	int INT_VIDEO
	jmp short fn_message

real_start:
	{store} mov bp,cx
	{store} mov bx,cx
	# BX = CX = BP = 0
	mov byte ptr [new_partition_type],PARTITION_TYPE_FAT32_LBA # Would be unnecessary if new_partition_type was set to FAT32.
	mov di,PARTITIONS_PTR+PARTITION_SIZE*3
	# DI: Partition pointer
	mov cx,0x4
reco_loop:
	cmp [di+PARTITION_TYPE],ch
	jz reco_loop_next
	call fn_read_boot_record_partition
	jne partition_type_check
	mov eax,0x4f434552          # 0x4f434552 == 'RECO'
	cmp [0x200+BS_OEMNAME],eax  # This can't be NTFS, because that normally requires BS_OEMNAME to be 'NTFS'.
	je reco_found
	cmp [0x200+0x1f0],eax
	jne partition_type_check
	mov byte ptr [new_partition_type],PARTITION_TYPE_NTFS
reco_found:
	{store} mov bx,di
	mov byte ptr [di+PARTITION_TYPE],PARTITION_TYPE_DIAGNOSTIC
partition_type_check:
	mov al,[di+PARTITION_TYPE]
	cmp al,PARTITION_TYPE_NTFS
	je partition_type_known
	cmp al,PARTITION_TYPE_FAT32
	je partition_type_known
	and al,0xff^0x0a
	cmp al,PARTITION_TYPE_FAT16 # Matches any FAT16, or FAT32 LBA.
	jne reco_loop_next
partition_type_known:
	{store} mov bp,di
reco_loop_next:
	mov [di+PARTITION_STATUS],ch
	sub di,PARTITION_SIZE
	loop reco_loop

	# BX = partition pointer for recovery partition
	# BP = partition pointer of first recognized partition type.

	# Using OR like TEST...
	{store} or bx,bx
	jz partition_find_boot
	{store} or bp,bp
	jz partition_find_boot

	# If there is a recovery partition and a FAT/NTFS partition
	{store} mov di,bx
	test byte ptr [flags],FLAG_FORCE_RECO
	jnz key_pressed
	test byte ptr [flags],FLAG_SKIP_KEY
	jnz key_timeout
	mov ah,KEYBOARD_CHECK_FOR_ENHANCED_KEYSTROKE
	int INT_KEYBOARD
	jnz key_available

	mov dl,[keyboard_timeout_seconds]
	{store} or dl,dl
	jz key_timeout
key_loop:
	dec dl
	js key_timeout

	# Tick frequency: (105 / 88) * 1*10^6 / 2^16 ~= 18.2 Hz
	# An 8-bit 'tick' counter overflows every 14.1 seconds.
	mov cl,[ss:0x400 + EBD_TIMER_TICKS_SINCE_MIDNIGHT]
	add cl,0x12                 # CL: timeout time; 0x12 ticks ~= 1 second
key_loop_second:
	# HLT belongs here.
	mov ah,KEYBOARD_CHECK_FOR_ENHANCED_KEYSTROKE
	int INT_KEYBOARD
	jnz key_available
	cmp cl,[ss:0x400 + EBD_TIMER_TICKS_SINCE_MIDNIGHT]
	jne key_loop_second
	jmp short key_loop

partition_find_boot:
	{store} xor edx,edx
	call fn_read_boot_record
	mov dx,0x1                  # DX: active partition not found
partition_loop0:
	mov cl,0x4
	mov di,0x200 + PARTITIONS_PTR
partition_loop1:
	{store} or dx,dx
	jnz not_found_yet
	cmp byte ptr [di+PARTITION_TYPE],0x0
	jne boot
not_found_yet:
	cmp byte ptr [di+PARTITION_STATUS],0x80
	je boot
	add di,PARTITION_SIZE
	loop partition_loop1
	dec dx
	jz partition_loop0
io_error:
	mov si,[msg_ind_err1]
general_error:
	call fn_message
	mov si,[msg_ind_err3]
	call fn_message
	mov ah,KEYBOARD_GET_KEYSTROKE
	int INT_KEYBOARD
	int INT_DISKLESS_BOOT_HOOK

key_available:
	mov ah,KEYBOARD_GET_ENHANCED_KEYSTROKE
	int INT_KEYBOARD
	# TODO: Are these codes right? Word is that on HP/Compaq it's F11 to enter recovery mode.
	cmp al,'r'
	je key_pressed
	cmp ah,0x80 | 0x05          # Keyboard break code '$/4'
	jne key_timeout
key_pressed:
	{store} mov bp,di
	jmp short change_partition_type

key_timeout:
	test byte ptr [flags],FLAG_CHANGE_PARTITION_TYPE
	jz write_mbr
change_partition_type:
	mov al,[new_partition_type]
	mov [di+PARTITION_TYPE],al
write_mbr:
	{store} mov di,bp
	mov byte ptr [di+PARTITION_STATUS],0x80
	and byte ptr [flags],0xff^0x06
	{store} xor edx,edx
	mov byte ptr [dap_transfer_buffer_byte],0x7a
	mov ah,INT13_EXTENDED_WRITE
	call fn_io_boot_record
boot:
	call fn_read_boot_record_partition
	mov si,[msg_ind_err2]
	jne general_error
	jmp 0x0:0x7c00

keyboard_timeout_seconds:
	.byte 0x01

flags:
	.byte FLAG_CHANGE_PARTITION_TYPE | 0x1

msg_ind_err2:
	.short msg_err2
msg_ind_err1:
	.short msg_err1
msg_ind_err3:
	.short msg_err3

	.short msg_press_f11        # Unused.

msg_err2:
	.asciz "Err2"
msg_err1:
	.asciz "\r\nErr1"
msg_err3:
	.asciz "Err3"

	# Unused past this point.
msg_press_f11:
	.asciz "\r\nPress F11 for Emergency Recovery "
	.asciz "s a key\r\n"
	.byte 0x00,0x6C,0x01

	.org 0x1fe
	.word 0xaa55
