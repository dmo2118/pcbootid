#!/bin/sh

# This file is part of pcbootid.

# pcbootid is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.

# pcbootid is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.

objdump -b binary -m i8086 -M intel --no-show-raw-insn -D --adjust-vma="$2" "$1" |
	sed -E 's/^[ ]*([0-9a-f]+:)/__\1/' |
	sed -E 's/(\t(j[a-z]+|call) +)0x([0-9a-f]+$)/\1_\3/' |
	sed -E 's/ *$//'
