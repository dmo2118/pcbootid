#!/bin/sh

# This file is part of pcbootid.

# pcbootid is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.

# pcbootid is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.

set -e

tmp="$(mktemp)"

in="$1"
addr="$2"
out="$(echo "$in" | sed 's/\.s$//')"

[ "$in" != "$out" ]

as --32 "$in" -o "$tmp"
ld --oformat binary -melf_i386 -Ttext="$addr" "$tmp" -o "$out"
chmod -x "$out"
rm "$tmp"
cmp -x "$out" "../image/$out"
