/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "os.h"

void print_error(error_status error, print_error_func print)
{
	TCHAR *message;
	DWORD size;

	size = FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
			FORMAT_MESSAGE_IGNORE_INSERTS |
			FORMAT_MESSAGE_FROM_SYSTEM |
			FORMAT_MESSAGE_MAX_WIDTH_MASK,
		NULL,
		error,
		0,
		(TCHAR *)&message,
		0,
		NULL);

	if(!size)
	{
		TCHAR str[12];
		wsprintf(str, (SHORT)error == (INT)error ? _T("%d ") : _T("%#.8X "), error);
		print(str);
	}
	else
	{
		print(message);
		LocalFree(message);
	}
}

error_status disk_read(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size)
{
	DWORD bytes_read;
	LARGE_INTEGER offset_ul;

	offset_ul.QuadPart = offset * sector_size;

	if(SetFilePointer(*disk, offset_ul.LowPart, &offset_ul.HighPart, FILE_BEGIN) == INVALID_SET_FILE_POINTER)
	{
		DWORD last_error = GetLastError();
		if(last_error)
			return last_error;
	}

	if(!ReadFile(*disk, buffer, size * sector_size, &bytes_read, NULL))
		return GetLastError();

	if(bytes_read != size * sector_size)
		return ERROR_HANDLE_EOF;
	return ERROR_SUCCESS;
}
