#!/usr/bin/env python3

# This file is part of pcbootid.

# pcbootid is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.

# pcbootid is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.

# Ew.

from __future__ import print_function

import hashlib
import json
import os
import re
import sys

# TODO: Do these two work?
def sort_and_combine(neg):
	neg.sort()
	neg = iter(neg)
	try:
		n = next(neg)

		while True:
			try:
				n2 = next(neg)
			except StopIteration:
				yield n
				break

			if n[1] >= n2[0]:
				if n2[1] > n[1]:
					n = (n[0], n2[1])
			else:
				yield n
				n = n2
	except StopIteration:
		pass

def sort_and_knockout(src, neg):
	src.sort()

	neg = iter(sort_and_combine(neg))
	n = None

	for s in src:
		try:
			while not n or n[1] <= s[0]:
				n = next(neg)

			while n[0] < s[1]:
				if s[0] != n[0]:
					yield (s[0], n[0])
				s = (n[1], s[1])
				n = next(neg)
		except StopIteration:
			pass

		if s[0] < s[1]:
			yield s

def sort_and_knockout_with(src, neg):
	src[:] = sort_and_knockout(src, neg)

def slice_by_n(a, n):
	return (a[i:i + n] for i in range(0, len(a), n))

def expr(s):
	if type(s) == str:
		return int(s, 0)
		# return sum(int(i, base = 0) for i in s.split('+'))
	return s

def expr_ranges_with(r):
	for i in range(len(r)):
		r[i] = tuple(map(expr, r[i]))
	return r

def hexn(n, digits = 2):
	return ('{:#0' + str(digits + 2) + 'x}').format(n)

def varname_from_filename(file_name):
	varname = re.sub('[^0-9A-Za-z_]', '_', file_name)
	# varname = re.sub('^(?=[0-9])', '_', varname)
	return varname

def print_to(out, *args, sep = ''):
	print(*args, file = out, sep = sep, end = '')

def print_bytes(out_c, tabs, data):
	BYTES_PER_LINE = 16
	for i in range(0, len(data), BYTES_PER_LINE):
		print_to(out_c, '\t' * tabs)
		print_to(out_c, *map(hexn, data[i:i + BYTES_PER_LINE]), sep = ', ')
		print_to(out_c, ',\n')

HASH_THRESHOLD = 32

def read_range(f, offset, r):
	begin, end = r
	f.seek(begin + offset)
	data = f.read(end - begin)
	assert len(data) == end - begin
	return begin, end, data

def print_signature_addr(out_c, begin, end, dst_offset):
	print_to(out_c, '{', hexn(begin - dst_offset), ', ', hexn(end - dst_offset), '}')

def print_signature_arrays(out_c, group_id, group_ext, file_name, src_offset, dst_offset, suffix, ranges):
	varname = varname_from_filename(file_name)

	ranges_set = ([], [])
	for r in ranges:
		r = tuple(map(expr, r))
		ranges_set[r[1] - r[0] >= HASH_THRESHOLD].append(r)

	if group_id:
		varname = group_id + '_' + varname
	if suffix:
		varname = varname + '_' + suffix

	if group_id:
		file_name += '.' + group_ext

	with open(os.path.join('image', file_name), 'rb') as f:
		if ranges_set[0]:
			for i in range(len(ranges_set[0])):
				begin, end, data = read_range(f, src_offset, ranges_set[0][i])
				print_to(out_c, 'static const uint8_t ', varname, '_literal', i, '[] =\n{\n')
				print_bytes(out_c, 1, data)
				print_to(out_c, '};\n\n')

			print_to(out_c, 'static const struct chunk_literal ', varname, '_literals[] =\n{\n')

			for i in range(len(ranges_set[0])):
				begin, end = ranges_set[0][i]
				print_to(out_c, '\t{')
				print_signature_addr(out_c, begin, end, dst_offset)
				print_to(out_c, ', ', varname, '_literal', i, '},\n')

			print_to(out_c, '};\n\n')

		if ranges_set[1]:
			print_to(out_c, 'static const struct chunk_hash ', varname, '_hashes[] =\n{\n')

			for r in ranges_set[1]:
				begin, end, data = read_range(f, src_offset, r)
				print_to(out_c, '\t{\n\t\t')
				print_signature_addr(out_c, begin, end, dst_offset)
				print_to(out_c, ',\n\t\t{\n')
				print_bytes(out_c, 3, hashlib.blake2b(data).digest())
				print_to(out_c, '\t\t}\n\t},\n')

			print_to(out_c, '};\n\n')

	return tuple(map(bool, ranges_set))

class Signature:
	@staticmethod
	def knockout(signature):
		return []

class SignatureBase(Signature):
	name = 'base'

	@staticmethod
	def print(out_c, tabs, varname, signature, has = '_has_ranges'):
		indent = '\t' * tabs
		sep = ',\n\t' + indent

		has_literals, has_hashes = signature[has]

		print_to(
			out_c,
			indent,
			'{\n\t',
			indent,
			varname + '_literals' if has_literals else 'NULL', # literals
			sep,
			varname + '_hashes' if has_hashes else 'NULL', # hashes
			sep,
			'arraysize(' + varname + '_literals)' if has_literals else '0', # literals
			sep,
			'arraysize(' + varname + '_hashes)' if has_hashes else '0', # hashes_size
			'\n',
			indent,
			'}')

class SignatureFull(Signature):
	name = 'full'

	@staticmethod
	def knockout(signature):
		str16 = signature.get('str16')
		if str16:
			return [(lambda a: (a, a + 2))(expr(a)) for a in str16['addrs']]
		return []

	@staticmethod
	def print(out_c, tabs, varname, signature):
		# TODO: Filesystems be part of the id.
		flags = [flag for flag in ['fat', 'fat32', 'ntfs', 'exfat', 'reserved_sectors', 'str_ff', 'str8_delta'] if signature.get(flag)]

		str8 = signature.get('str8')
		str16 = signature.get('str16', {'base': 0, 'addrs': []})
		str16_addrs = str16['addrs']

		indent = '\t' * tabs

		sep = ',\n\t' + indent
		print_to(out_c, indent, '{\n')

		SignatureBase.print(out_c, tabs + 1, varname, signature)

		print_to(
			out_c,
			sep,
			'_T("',
			re.sub(r'([\"])', r'\\\1', signature['system']), # system # TODO: Needs better escaping here.
			'")',
			sep,
			(varname + '_str16' if str16_addrs else 'NULL'), # str16
			sep,
			(hexn(expr(str8['addr']), 3) if str8 else '0'), # str8_base
			# sep,
			# hexn(expr((str8 and str8.get('base')) or 0x100), 3), # str8_base
			sep,
			hexn(expr(str16['base']), 3), # str16_base
			sep,
			(expr(str8['size']) if str8 else 0), # str8_size
			sep,
			('arraysize(' + varname + '_str16)' if str16_addrs else '0'), # str16_size
			sep,
			' | '.join('FLAG_' + flag.upper() for flag in flags) if flags else '0', # flags
			'\n',
			indent,
			'}')

class SignatureGRUB2Boot(Signature):
	name = 'grub2_boot'

	@staticmethod
	def knockout(signature):
		boot_drive = expr(signature['boot_drive'])
		result = [(boot_drive, boot_drive + 1)]

		core_sector32 = expr(signature.get('core_sector32'))
		if core_sector32:
			result.append((core_sector32, core_sector32 + 4))

		core_sector = expr(signature.get('core_sector'))
		if core_sector:
			result.append((core_sector, core_sector + 8))

		if signature.get('has_boot_drive_check', True):
			after_bpb = expr(signature['after_bpb'])
			result.append((after_bpb + 1, after_bpb + 3))

		return result

	@staticmethod
	def print(out_c, tabs, varname, signature):
		indent = '\t' * tabs
		sep = ',\n\t' + indent
		print_to(out_c, indent, '{\n')
		SignatureFull.print(out_c, tabs + 1, varname, signature)
		print_to(out_c, ',\n')
		SignatureBase.print(out_c, tabs + 1, varname + '_floppy', signature, '_has_floppy')
		print_to(
			out_c,
			sep,
			hexn(expr(signature.get('core_sector') or signature.get('core_sector32'))),
			sep,
			hexn(expr(signature['boot_drive'])),
			sep,
			hexn(expr(signature['after_bpb'])),
			',\n',
			indent,
			'}')

class SignatureGRUB19Kernel(Signature):
	name = 'grub1_9_kernel'

	def print(out_c, tabs, varname, signature):
		indent = '\t' * tabs
		sep = ',\n\t' + indent
		print_to(out_c, indent, '{\n')
		SignatureFull.print(out_c, tabs + 1, varname, signature)
		print_to(
			out_c,
			'}')

class SignatureGRUB2LZMADecompress(Signature):
	name = 'grub2_lzma_decompress'

	def knockout(signature):
		call_grub_gate_a20 = expr(signature['call_grub_gate_a20'])
		add_decompressor_end_reed_solomon_part = expr(signature['add_decompressor_end_reed_solomon_part'])
		lea_reed_solomon_part = expr(signature['lea_reed_solomon_part'])
		call_grub_reed_solomon_recover = expr(signature['call_grub_reed_solomon_recover'])
		jmp_post_reed_solomon = expr(signature['jmp_post_reed_solomon'])
		return [
			(call_grub_gate_a20, call_grub_gate_a20 + 4),
			(add_decompressor_end_reed_solomon_part, add_decompressor_end_reed_solomon_part + 4),
			(lea_reed_solomon_part, lea_reed_solomon_part + 4),
			(call_grub_reed_solomon_recover, call_grub_reed_solomon_recover + 4),
			(jmp_post_reed_solomon, jmp_post_reed_solomon + 4)]

	def print(out_c, tabs, varname, signature):
		indent = '\t' * tabs
		sep = ',\n\t' + indent
		print_to(out_c, indent, '{\n')
		SignatureFull.print(out_c, tabs + 1, varname, signature)
		print_to(out_c, ',\n')
		SignatureBase.print(out_c, tabs + 1, varname + '_grub_gate_a20', signature, '_has_grub_gate_a20')
		print_to(out_c, ',\n')
		SignatureBase.print(out_c, tabs + 1, varname + '_post_reed_solomon', signature, '_has_post_reed_solomon')
		print_to(
			out_c,
			sep,
			hexn(expr(signature['call_grub_gate_a20'])),
			sep,
			hexn(expr(signature['jmp_post_reed_solomon'])),
			sep,
			hexn(expr(signature['post_reed_solomon_decompressor_end'])),
			',\n',
			indent,
			'}')

class SignatureGRUB2Kernel(Signature):
	name = 'grub2_kernel'

	# TODO: Knockout.

	def print(out_c, tabs, varname, signature):
		indent = '\t' * tabs
		sep = ',\n\t' + indent
		print_to(out_c, indent, '{\n')
		SignatureFull.print(out_c, tabs + 1, varname, signature)
		print_to(
			out_c,
			sep,
			hexn(expr(signature['size'])),
			',\n',
			indent,
			'}')

def main(out_h, out_c):
	hashes_in = json.load(sys.stdin)
	sig_classes = {
		c.name: c
		for c
		in [
			SignatureBase,
			SignatureFull,
			SignatureGRUB2Boot,
			SignatureGRUB19Kernel,
			SignatureGRUB2LZMADecompress,
			SignatureGRUB2Kernel
		]
	}

	print_to(out_h, '#ifndef HASHES_H\n#define HASHES_H\n\n#include "sig.h"\n\n')

	print_to(out_c, '#include "hashes.h"\n\n')

	for group in hashes_in:
		group_id = group['id']
		group_class = group.get('class', group_id)
		group_ext = group.get('ext', group_id)
		group_src_offset = expr(group.get('src_offset', 0))
		group_dst_offset = expr(group.get('dst_offset', 0))

		for signature in group['signatures']:
			sig_class = sig_classes[group_class or signature['class']]
			file_name = signature['id']
			src_offset = expr(signature.get('src_offset')) or group_src_offset
			dst_offset = expr(signature.get('dst_offset')) or group_dst_offset

			if group_id and '_source' not in signature:
				print(group_id, '/', file_name, ': no source', file = sys.stderr)

			ranges = signature['ranges']
			expr_ranges_with(ranges)
			sort_and_knockout_with(ranges, sig_class.knockout(signature))

			signature['_has_ranges'] = print_signature_arrays(
				out_c,
				group_id,
				group_ext,
				file_name,
				src_offset,
				dst_offset,
				None,
				signature['ranges'])

			str16 = signature.get('str16')
			if str16:
				str16_addrs = str16['addrs']
				if str16_addrs:
					print_to(
						out_c,
						'static const uint16_t ',
						group_id,
						'_',
						varname_from_filename(file_name),
						'_str16[] = {',
						', '.join(hexn(expr(i), 3) for i in str16_addrs),
						'};\n\n')

			# TODO: Does this still good?
			if group_id == 'grub2_boot':
				signature['_has_floppy'] = print_signature_arrays(
					out_c,
					group_id,
					group_ext,
					file_name,
					src_offset,
					dst_offset,
					'floppy',
					signature['floppy'])

			if group_id == 'grub2_lzma_decompress':
				grub_gate_a20 = signature['grub_gate_a20']
				expr_ranges_with(grub_gate_a20)

				grub_gate_a20_prot_to_real = expr(signature['grub_gate_a20_prot_to_real']) + grub_gate_a20[0][0]
				grub_gate_a20_real_to_prot = expr(signature['grub_gate_a20_real_to_prot']) + grub_gate_a20[0][0]
				sort_and_knockout_with(
					grub_gate_a20,
					[
						(grub_gate_a20_prot_to_real, grub_gate_a20_prot_to_real + 4),
						(grub_gate_a20_real_to_prot, grub_gate_a20_real_to_prot + 4)
					])

				signature['_has_grub_gate_a20'] = print_signature_arrays(
					out_c,
					group_id,
					group_ext,
					file_name,
					src_offset,
					min(a for a, b in grub_gate_a20),
					'grub_gate_a20',
					grub_gate_a20)

				post_reed_solomon = signature['post_reed_solomon']
				expr_ranges_with(post_reed_solomon)

				decompressor_end = expr(signature['post_reed_solomon_decompressor_end']) + post_reed_solomon[0][0]
				sort_and_knockout_with(
					post_reed_solomon,
					[(decompressor_end, decompressor_end + 4)])

				signature['_has_post_reed_solomon'] = print_signature_arrays(
					out_c,
					group_id,
					group_ext,
					file_name,
					src_offset,
					min(a for a, b in post_reed_solomon),
					'post_reed_solomon',
					post_reed_solomon)

		if group_id:
			print_to(out_h, 'enum\n{\n')
			for signature in group['signatures']:
				print_to(out_h, '\t', (group_id + '_' + varname_from_filename(signature['id'])).upper(), ',\n')
			print_to(out_h, '};\n\n')

			group_def = ['const struct signature_', group_class, ' ', group_id, '[', len(group['signatures']) ,']']

			print_to(out_h, 'extern ', *group_def, ';\n\n')
			print_to(out_c, *group_def, ' =\n{\n')

		for signature in group['signatures']:
			sig_class = sig_classes[group_class or signature['class']]

			print_signature_fn = sig_class.print

			varname = varname_from_filename(signature['id'])
			if group_id:
				varname = group_id + '_' + varname
			else:
				sig_def = ['const struct signature_', sig_class.name, ' ', varname]
				print_to(out_h, 'extern ', *sig_def, ';\n')
				print_to(out_c, *sig_def, ' =\n')

			print_signature_fn(out_c, bool(group_id), varname, signature)

			if group_id:
				print_to(out_c, ',\n')
			else:
				print_to(out_c, ';\n\n')

		if group_id:
			print_to(out_c, '};\n\n')

	print_to(out_h, '\n#endif\n')

main(open('hashes.h', 'w'), open('hashes.c', 'w')) # TODO: Write to temp files first.
