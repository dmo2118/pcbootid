/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "os.h"

error_status disk_read(const disk_handle *disk, void *buffer, uint64_t offset, unsigned size)
{
	ssize_t bytes_read;
	if(lseek(*disk, offset * sector_size, SEEK_SET) < 0)
		return errno;

	bytes_read = read(*disk, buffer, size * sector_size);
	if(bytes_read < 0)
		return errno;
	if(bytes_read != size * sector_size)
		return EIO;
	return 0;
}
