/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#define __STDC_WANT_LIB_EXT1__ 1 /* TR 24731, for OpenWatcom strnlen_s */

#include "sig.h"

#if __WATCOMC__
#	define strnlen strnlen_s
#endif

void blake2b512(uint8_t *out, const void *in, size_t inlen)
{
#if OPENSSL_VERSION_NUMBER >= 0x10100000L
	EVP_MD_CTX *ctx = EVP_MD_CTX_new();
	if(!ctx)
		abort(); // TODO: Maybe no abort()?
	EVP_DigestInit(ctx, EVP_blake2b512());
	EVP_DigestUpdate(ctx, in, inlen);
	EVP_DigestFinal(ctx, out, NULL);
#else
	blake2b(out, in, NULL, BLAKE2B_OUTBYTES, inlen, 0);
#endif
}

bool str_valid(const void *sector, unsigned offset, bool ff)
{
	size_t max_length;
	bool result = false;
	const char *str = (const char *)sector + offset;
	size_t len;

	if(offset <= sector_size - 2)
	{
		max_length = sector_size - offset;

		if(ff)
		{
			len = 0;
			while(len != max_length)
			{
				unsigned char c = str[len];
				if(!c || c == 0xff)
					break;
				++len;
			}
		}
		else
		{
			len = strnlen(str, max_length);
		}

		result = len != max_length;
	}

	_fputts(_T("  - "), stdout);

	if(result)
	{
		// TODO: Line wrap for newlines.
		_puttchar(_T('"'));
		while(len)
		{
			print_yaml_char_oem(*str);
			++str;
			--len;
		}
		_puttchar(_T('"'));
	}

	_puttchar(_T('\n'));

	return result;
}

bool str16_valid(const void *sector, unsigned offset, unsigned base, bool ff)
{
	return str_valid(sector, _little_endian_load_16((const char *)sector + offset) - base, ff);
}

unsigned signature_begin(const struct signature_base *sig)
{
	unsigned literal_begin = -1, hash_begin = -1;
	if(sig->literals)
		literal_begin = sig->literals[0].addr.begin;
	if(sig->hashes)
		hash_begin = sig->hashes[0].addr.begin;
	return literal_begin < hash_begin ? literal_begin : hash_begin;
}

unsigned signature_end(const struct signature_base *sig)
{
	unsigned literal_end = 0, hash_end = 0;
	if(sig->literals)
		literal_end = sig->literals[sig->literals_size - 1].addr.end;
	if(sig->hashes)
		hash_end = sig->hashes[sig->hashes_size - 1].addr.end;
	return literal_end > hash_end ? literal_end : hash_end;
}

bool check_signature_base(const struct signature_base *sig, const void *sector)
{
	unsigned i;

	// _debug_hex(sector);

	for(i = 0; i != sig->literals_size; ++i)
	{
		const struct chunk_literal *chunk = sig->literals + i;
		if(memcmp((const unsigned char *)sector + chunk->addr.begin, chunk->data, chunk->addr.end - chunk->addr.begin))
			return false;
	}

	for(i = 0; i != sig->hashes_size; ++i)
	{
		uint8_t out[BLAKE2B_OUTBYTES];
		const struct chunk_hash *chunk = sig->hashes + i;

		blake2b512(out, (const unsigned char *)sector + chunk->addr.begin, chunk->addr.end - chunk->addr.begin);

		if(memcmp(out, chunk->blake2b, sizeof(out)))
			return false;
	}

	return true;
}

bool check_signature(const _TCHAR *class, const struct signature_full *sig, uint64_t sector_offset, const void *sector)
{
	if(!check_signature_base(&sig->base, sector))
		return false;

	print_sector(sig->system, class, sector_offset);

	if(sig->flags & FLAG_NTFS)
		print_bool(_T("OEM ID OK"), !memcmp((const char *)sector + 3, "NTFS", 4)); /* Unaligned 32-bit value. */

	if(sig->flags & (FLAG_FAT | FLAG_FAT32 | FLAG_NTFS))
	{
		// Convenient that this is the same on FAT and NTFS. Not present on exFAT.
		print_bool(_T("Hidden sectors OK"), sector_offset == *(const uint32_t *)((char *)sector + 0x1c));
	}

	if(sig->str8_n || sig->str16_size)
		strings_key();

	unsigned n = sig->str8_n;
	unsigned offset = sig->str8_offset;
	while(n)
	{
		str_valid(
			sector,
			((const uint8_t *)sector)[offset] + (sig->flags & FLAG_STR8_DELTA ? offset + 1 : 0x100),
			sig->flags & FLAG_STR_FF);
		++offset;
		--n;
	}

	for(unsigned str = 0; str != sig->str16_size; ++str)
		str16_valid(sector, sig->str16[str], sig->str16_base, sig->flags & FLAG_STR_FF);

	return true;
}
