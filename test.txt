--- # 1.90.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.90
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: 
  Boot drive check OK: false
  INT 13H boot drive: 
- System: 1.90
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 0
    Address: 0x8200
- System: 
  Class: GRUB core.img
  Sector: 2
--- # 1.91.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.91
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: 
  Boot drive check OK: false
  INT 13H boot drive: 
- Class: Error
  Error: "Input/output error"
--- # 1.93.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.93
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: 
  Boot drive check OK: false
  INT 13H boot drive: 
- Class: Error
  Error: "Input/output error"
--- # 1.94.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.94
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: 
  Boot drive check OK: false
  INT 13H boot drive: 
- Class: Error
  Error: "Input/output error"
--- # 1.95.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.95
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: 
  Boot drive check OK: false
  INT 13H boot drive: 
- System: 1.95
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 0
    Address: 0x8200
- System: 
  Class: GRUB core.img
  Sector: 2
--- # 1.97.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.97
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: 
  Boot drive check OK: false
  INT 13H boot drive: 
- System: 1.97
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 0
    Address: 0x8200
- System: 
  Class: GRUB core.img
  Sector: 2
--- # 1.99.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.99
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: false
  Boot drive check OK: true
  INT 13H boot drive: 
- System: 1.97
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 36
    Address: 0x8200
- System: 
  Class: GRUB core.img
  Sector: 2
--- # 2.00.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 1.99
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: false
  Boot drive check OK: true
  INT 13H boot drive: 
- System: 1.97
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 39
    Address: 0x8200
- System: 2.00
  Class: GRUB core.img (lzma_decompress.img)
  Sector: 2
  grub_gate_a20: 0x8948
  post_reed_solomon: 0x8a6e
  decompressor_end: 0x8dd0
  Compressed size: 16784
  Uncompressed size: 28856
- System: 2.00
  Class: GRUB core.img (kernel.img)
  Kernel size: 28832
  Modules size OK: true
  Modules magic OK: true
  Modules:
  - Type: PREFIX
    Size: 4
    Value: "."
# GRUB code written in C cannot be verified.
--- # 2.00.grub2_kernel
# Not a disk. Use -h and -s to check CHS geometry.
- System: ?
  Sector: 0
  55AA signature OK: false
--- # 2.02-beta1.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 2.02
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: false
  Boot drive check OK: true
  INT 13H boot drive: 
- System: 1.97
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 37
    Address: 0x8200
- System: 2.02-beta1
  Class: GRUB core.img (lzma_decompress.img)
  Sector: 2
  grub_gate_a20: 0x88d1
  post_reed_solomon: 0x89e2
  decompressor_end: 0x8d40
  Compressed size: 15713
  Uncompressed size: 27724
- System: 2.00
  Class: GRUB core.img (kernel.img)
  Kernel size: 27684
  Modules size OK: true
  Modules magic OK: true
  Modules:
  - Type: PREFIX
    Size: 20
    Value: "(,msdos1)/boot/grub"
# GRUB code written in C cannot be verified.
--- # 2.02.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 2.02
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: false
  Boot drive check OK: true
  INT 13H boot drive: 
- System: 1.97
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 37
    Address: 0x8200
- System: 2.02
  Class: GRUB core.img (lzma_decompress.img)
  Sector: 2
  grub_gate_a20: 0x88cc
  post_reed_solomon: 0x89de
  decompressor_end: 0x8d40
  Compressed size: 15994
  Uncompressed size: 28120
- System: 2.02
  Class: GRUB core.img (kernel.img)
  Kernel size: 28096
  Modules size OK: true
  Modules magic OK: true
  Modules:
  - Type: PREFIX
    Size: 4
    Value: "."
# GRUB code written in C cannot be verified.
--- # 2.02.grub2_kernel
# Not a disk. Use -h and -s to check CHS geometry.
- System: ?
  Sector: 0
  55AA signature OK: false
--- # acronis.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Acronis True Image
  Class: MBR
  Sector: 0
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 
--- # debian_2.02-1.grub2
# Not a disk. Use -h and -s to check CHS geometry.
- System: 2.02-1 (Debian)
  Class: GRUB boot.img
  Sector: 0
  Disk: floppy
  Boot drive check: false
  Boot drive check OK: true
  INT 13H boot drive: 
- System: 2.02-1 (Debian)
  Class: GRUB core.img (diskboot.img)
  Sector: 1
  Blocklist block count OK: true
  Blocklist:
  - Sector: 2
    Length: 0
    Address: 0x8200
- System: 
  Class: GRUB core.img
  Sector: 2
--- # dos.fat.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: MS-DOS 5.0
  Class: VBR
  Sector: 0
  Hidden sectors OK: true
  Strings:
  - "\r\nNon-System disk or disk error\r\nReplace and press any key when ready\r\n"
  55AA signature OK: true
  Filesystem: FAT
- Class: File
  Files:
  - "IO.SYS"
  - "MSDOS.SYS"
--- # dos.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: MS-DOS 3.30
  Class: MBR
  Sector: 0
  Strings:
  - "Invalid partition table"
  - "Error loading operating system"
  - "Missing operating system"
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 1
- Class: Error
  Error: "Input/output error"
--- # hp_compaq.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: HP/Compaq (~2007)
  Class: MBR
  Sector: 0
  Strings:
  - "Err2"
  - "\r\nErr1"
  - "Err3"
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 
--- # mkfs.fat.pi
# Not a disk. Use -h and -s to check CHS geometry.
- System: mkfs.fat/mkfs.ntfs (not bootable)
  Sector: 0
  Strings:
  - "This is not a bootable disk.  Please insert a bootable floppy and\r\npress any key to try again ... \r\n"
--- # partition_wizard.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: MiniTool Partition Wizard
  Class: MBR
  Sector: 0
  Strings:
  - "Error!"
  - "Error!"
  - "Error!"
  55AA signature OK: true
  ID: "D210A615-ACFD-414a-BDF1-FC9F2A85F076"
  Active partitions <= 1 OK: true
  Active partition: 
--- # testdisk.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: TestDisk
  Class: MBR
  Sector: 0
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 
--- # windows_2000.fat32.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 2000
  Class: VBR
  Sector: 0
  Hidden sectors OK: false
  Strings:
  - "\r\nNTLDR is missing"
  - "\r\nDisk error"
  - "\r\nPress any key to restart\r\n"
  55AA signature OK: true
  Filesystem: FAT32
  Logical sector 12 OK: true
- Class: File
  Files:
  - "NTLDR"
--- # windows_2000.fat.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 2000
  Class: VBR
  Sector: 0
  Hidden sectors OK: true
  Strings:
  - "\r\nRemove disks or other media."
  - "\r\nDisk error"
  - "\r\nPress any key to restart\r\n"
  55AA signature OK: true
  Filesystem: FAT
- Class: File
  Files:
  - "NTLDR"
--- # windows_2000.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 2000
  Class: MBR
  Sector: 0
  Strings:
  - "Invalid partition table"
  - "Error loading operating system"
  - "Missing operating system"
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 1
- Class: Error
  Error: "Input/output error"
--- # windows_2000_no_chs.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 2000 (CHS disabled)
  Class: MBR
  Sector: 0
  Strings:
  - "j"
  - "j"
  - "j"
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 
--- # windows_2000.ntfs.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 2000
  Class: VBR
  Sector: 0
  OEM ID OK: true
  Hidden sectors OK: false
  Strings:
  - "\r\nA disk read error occurred"
  - "\r\nNTLDR is missing"
  - "\r\nNTLDR is compressed"
  - "\r\nPress Ctrl+Alt+Del to restart\r\n"
  55AA signature OK: true
  Filesystem: NTFS
  Version: KB320397
- Class: File
  Files:
  - "NTLDR"
--- # windows_2000_sp0.ntfs.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 2000
  Class: VBR
  Sector: 0
  OEM ID OK: true
  Hidden sectors OK: false
  Strings:
  - "\r\nA disk read error occurred"
  - "\r\nNTLDR is missing"
  - "\r\nNTLDR is compressed"
  - "\r\nPress Ctrl+Alt+Del to restart\r\n"
  55AA signature OK: true
  Filesystem: NTFS
  Version: RTM
- Class: File
  Files:
  - "NTLDR"
--- # windows_7.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- Class: Error
  Error: "Input/output error"
--- # windows_7.ntfs.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 7
  Class: VBR
  Sector: 0
  OEM ID OK: true
  Hidden sectors OK: false
  Strings:
  - "\r\nA disk read error occurred"
  - "\r\nBOOTMGR is missing"
  - "\r\nBOOTMGR is compressed"
  - "\r\nPress Ctrl+Alt+Del to restart\r\n"
  55AA signature OK: true
  Filesystem: NTFS
  $Boot OK: true
- Class: File
  Files:
  - "BOOTMGR"
  - "NTLDR"
--- # windows_8.exfat.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 8
  Class: VBR
  Sector: 0
  Strings:
  - "\r\nRemove disks or other media."
  - "\r\nDisk error"
  - "\r\nPress any key to restart\r\n"
  55AA signature OK: true
  Filesystem: exFAT
  Extended boot sectors OK: true
- Class: File
  Files:
  - "BOOTMGR"
--- # windows_8.fat32.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 8
  Class: VBR
  Sector: 0
  Hidden sectors OK: true
  55AA signature OK: true
  Filesystem: FAT32
  Strings:
  - "\r\nDisk error"
  - "\r\nPress any key to restart\r\n"
  - "\r\nAn operating system wasn't found. Try disconnecting any drives that don't\r\ncontain an operating system."
  Logical sector 12 OK: true
- Class: File
  Files:
  - "BOOTMGR"
--- # windows_8.ntfs.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 8
  Class: VBR
  Sector: 0
  OEM ID OK: true
  Hidden sectors OK: false
  Strings:
  - "\r\nA disk read error occurred"
  - "\r\nBOOTMGR is compressed"
  - "\r\nPress Ctrl+Alt+Del to restart\r\n"
  55AA signature OK: true
  Filesystem: NTFS
--- # windows_95.fat32.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 95
  Class: VBR
  Sector: 0
  Hidden sectors OK: false
  Strings:
  - "\r\nInvalid system disk"
  - "\r\nDisk I/O error"
  - "\r\nInvalid system disk"
  - "\r\nReplace the disk, and then press any key\r\n"
  55AA signature OK: true
  Filesystem: FAT32
  Logical sector 2 OK: true
- Class: File
  Files:
  - "IO.SYS"
  - "MSDOS.SYS"
  - "WINBOOT.SYS"
--- # windows_95.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 95
  Class: MBR
  Sector: 0
  55AA signature OK: true
  Strings:
  - "Invalid partition table"
  - "Error loading operating system"
  - "Missing operating system"
  Active partitions <= 1 OK: true
  Active partition: 1
- Class: Error
  Error: "Input/output error"
--- # windows_98.fat.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows 98
  Class: VBR
  Sector: 0
  Hidden sectors OK: true
  Strings:
  - "\r\nInvalid system disk"
  - "\r\nDisk I/O error"
  - "\r\nInvalid system disk"
  - "\r\nReplace the disk, and then press any key\r\n"
  55AA signature OK: true
  Filesystem: FAT
- Class: File
  Files:
  - "IO.SYS"
  - "MSDOS.SYS"
--- # windows_vista.fat32.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows Vista
  Class: VBR
  Sector: 0
  Hidden sectors OK: true
  Strings:
  - "\r\nRemove disks or other media."
  - "\r\nDisk error"
  - "\r\nPress any key to restart\r\n"
  55AA signature OK: true
  Filesystem: FAT32
  Logical sector 12 OK: true
- Class: File
  Files:
  - "BOOTMGR"
--- # windows_vista.mbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows Vista
  Class: MBR
  Sector: 0
  Strings:
  - "Invalid partition table"
  - "Error loading operating system"
  - "Missing operating system"
  55AA signature OK: true
  Active partitions <= 1 OK: true
  Active partition: 1
- Class: Error
  Error: "Input/output error"
--- # windows_vista.ntfs.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows Vista
  Class: VBR
  Sector: 0
  OEM ID OK: true
  Hidden sectors OK: false
  Strings:
  - "\r\nA disk read error occurred"
  - "\r\nBOOTMGR is missing"
  - "\r\nBOOTMGR is compressed"
  - "\r\nPress Ctrl+Alt+Del to restart\r\n"
  55AA signature OK: true
  Filesystem: NTFS
  $Boot OK: true
- Class: File
  Files:
  - "BOOTMGR"
  - "NTLDR"
--- # windows_xp.exfat.vbr
# Not a disk. Use -h and -s to check CHS geometry.
- System: Windows XP
  Class: VBR
  Sector: 0
  Strings:
  - "\r\nRemove disks or other media."
  - "\r\nDisk error"
  - "\r\nPress any key to restart\r\n"
  55AA signature OK: true
  Filesystem: exFAT
