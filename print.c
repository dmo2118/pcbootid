/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "print.h"

/*
static int _debug_memcmp(const void *buf1, const void *buf2, size_t size)
{
	const char *b1 = buf1;
	const char *b2 = buf2;
	size_t i;
	for(i = 0; i != size; ++i)
	{
		int diff = b2[i] - b1[i];
		if(diff)
		{
			_ftprintf(stderr, _T("_debug_memcmp @ 0x%x\n"), i);
			return diff;
		}
	}

	return 0;
}
*/

/*
static void _debug_hex(const void *sector)
{
	const uint8_t *s = sector;
	unsigned addr;
	for(addr = 0; addr != 512; addr += 16)
	{
		unsigned x;

		printf("%.4x  ", addr);
		for(x = 0; x != 16; ++x)
			printf("%.2x ", s[addr + x]);

		putchar('|');

		for(x = 0; x != 16; ++x)
		{
			uint8_t c = s[addr + x];
			putchar(!c ? ' ' : (c >= 0x20 && c < 0x7f ? c : '.'));
		}

		putchar('|');
		putchar('\n');
	}
}
*/

void print_yaml_char(uint32_t c)
{
	const _TCHAR *s;
	switch(c)
	{
	case '\0':
		s = _T("\\0");
		break;
	case '\a':
		s = _T("\\a");
		break;
	case '\t':
		s = _T("\\t");
		break;
	case '\n':
		s = _T("\\n");
		break;
	case '\v':
		s = _T("\\v");
		break;
	case '\f':
		s = _T("\\f");
		break;
	case '\r':
		s = _T("\\r");
		break;
	case '"':
		s = _T("\\\"");
		break;
	case '\\':
		s = _T("\\\\");
		break;
	default:
		if(c >= 0x20 && c < 0x7f)
		{
			_puttchar(c);
		}
		else
		{
			// TODO: Print UTF-8 directly if the console supports it.
			const _TCHAR *fmt;
			if(c > 0x10000)
				fmt = _T("\\U%.8x");
			else if(c > 0x100)
				fmt = _T("\\u%.4x");
			else
				fmt = _T("\\x%.2x");
			_tprintf(fmt, c);
		}
		return;
	}

	_fputts(s, stdout);
}

void print_yaml_char_oem(unsigned char c)
{
	static const uint16_t c0[] =
	{
		0x0000, 0x263a, 0x263b, 0x2665, 0x2666, 0x2663, 0x2660, 0x0007,
		0x25d8, 0x0009, 0x000a, 0x000b, 0x000c, 0x000d, 0x266b, 0x263c,
		0x25ba, 0x25c4, 0x2195, 0x203c, 0x00b6, 0x00a7, 0x25ac, 0x21a8,
		0x2191, 0x2193, 0x2192, 0x2190, 0x221f, 0x001d, 0x001e, 0x001f,
	};

	static const uint16_t c1[] =
	{
		0x00c7, 0x00fc, 0x00e9, 0x00e2, 0x00e4, 0x00e0, 0x00e5, 0x00e7,
		0x00ea, 0x00eb, 0x00e8, 0x00ef, 0x00ee, 0x00ec, 0x00c4, 0x00c5,
		0x00c9, 0x00e6, 0x00c6, 0x00f4, 0x00f6, 0x00f2, 0x00fb, 0x00f9,
		0x00ff, 0x00d6, 0x00dc, 0x00a2, 0x00a3, 0x00a5, 0x20a7, 0x0192,
		0x00e1, 0x00ed, 0x00f3, 0x00fa, 0x00f1, 0x00d1, 0x00aa, 0x00ba,
		0x00bf, 0x2310, 0x00ac, 0x00bd, 0x00bc, 0x00a1, 0x00ab, 0x00bb,
		0x2591, 0x2592, 0x2593, 0x2502, 0x2524, 0x2561, 0x2562, 0x2556,
		0x2555, 0x2563, 0x2551, 0x2557, 0x255d, 0x255c, 0x255b, 0x2510,
		0x2514, 0x2534, 0x252c, 0x251c, 0x2500, 0x253c, 0x255e, 0x255f,
		0x255a, 0x2554, 0x2569, 0x2566, 0x2560, 0x2550, 0x256c, 0x2567,
		0x2568, 0x2564, 0x2565, 0x2559, 0x2558, 0x2552, 0x2553, 0x256b,
		0x256a, 0x2518, 0x250c, 0x2588, 0x2584, 0x258c, 0x2590, 0x2580,
		0x03b1, 0x00df, 0x0393, 0x03c0, 0x03a3, 0x03c3, 0x00b5, 0x03c4,
		0x03a6, 0x0398, 0x03a9, 0x03b4, 0x221e, 0x03c6, 0x03b5, 0x2229,
		0x2261, 0x00b1, 0x2265, 0x2264, 0x2320, 0x2321, 0x00f7, 0x2248,
		0x00b0, 0x2219, 0x00b7, 0x221a, 0x207f, 0x00b2, 0x25a0, 0x00a0,
	};

	wchar_t wc;

	if(c >= 0x80)
		wc = c1[c - 0x80];
	else if(c < 0x20)
		wc = c0[c];
	else if(c == 0x7f)
		wc = 0x2302;
	else
		wc = c;

	print_yaml_char(wc);
}

void print_error_yaml_prefix(void)
{
	_fputts(_T("- Class: Error\n  Error: "), stdout);
}

static void _print_yaml(const _TCHAR *str)
{
#if !_UNICODE
	mbtowc(NULL, NULL, 0);
#endif

	for(;;)
	{
		wchar_t c;
		// TODO: Win32 here? DOS here?
#if _UNICODE
		c = *str; // TODO: This botches surrogate pairs.
		if(!c)
			break;
		++str;
#else
		int result = mbtowc(&c, str, MB_CUR_MAX);
		if(!result)
			break;

		if(result < 0)
		{
			c = 0xfffd;
			result = 1;
		}

		str += result;
#endif
		print_yaml_char(c);
	}
}

void print_error_yaml(error_status error)
{
	// TODO: Also print the numeric value.
	print_error_yaml_prefix();
	_puttchar(_T('"'));
	print_error(error, _print_yaml);
	_putts(_T("\""));
}

void print_sector(const _TCHAR *system, const _TCHAR *class, uint64_t sector)
{
	// TODO: MSVC uint64_t printf
	_tprintf(_T("- System: %s\n"), system);
	if(class)
		_tprintf(_T("  Class: %s\n"), class);
	if(sector != ~0ull)
		_tprintf(_T("  Sector: %llu\n"), (unsigned long long)sector);
}

void print_bool(const _TCHAR *prefix, bool x)
{
	_tprintf(_T("  %s: %s\n"), prefix, x ? _T("true") : _T("false"));
}

void strings_key()
{
	_putts(_T("  Strings:"));
}
