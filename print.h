/*
This file is part of pcbootid.

pcbootid is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

pcbootid is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with pcbootid.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef PRINT_H
#define PRINT_H

#include "os.h"

void print_yaml_char(uint32_t c);
void print_yaml_char_oem(unsigned char c);

void print_error_yaml_prefix(void);
void print_error_yaml(error_status error);
void print_sector(const _TCHAR *system, const _TCHAR *class, uint64_t sector);
void print_bool(const _TCHAR *prefix, bool x);

void strings_key();

#endif
