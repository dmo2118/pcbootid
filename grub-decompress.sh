#!/bin/sh

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3 of the License.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

if [ "$#" != 2 ]
then
	echo "Usage: $0 address decompressor_end < core.img"
	exit 1
fi

address=$(($1))
decompressor_end=$(($2))
offset=$(($decompressor_end + 512 - $address))
dd bs=1 skip="$offset" | lzma -dF raw --lzma1=dict=65536,lc=3,lp=0,pb=2
